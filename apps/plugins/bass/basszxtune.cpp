/*************************************************************************}
{ basszxtune.cpp - BASS sound library plugin                              }
{                                                                         }
{ (c) Alexey Parfenov, 2014                                               }
{                                                                         }
{ e-mail: zxed@alkatrazstudio.net                                         }
{                                                                         }
{ This program is free software; you can redistribute it and/or           }
{ modify it under the terms of the GNU General Public License             }
{ as published by the Free Software Foundation; either version 3 of       }
{ the License, or (at your option) any later version.                     }
{                                                                         }
{ This program is distributed in the hope that it will be useful,         }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of          }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        }
{ General Public License for more details.                                }
{                                                                         }
{ You may read GNU General Public License at:                             }
{   http://www.gnu.org/copyleft/gpl.html                                  }
{*************************************************************************/

#include "../common/pluginbase.h"
#include "bass-addon.h"

#include "basszxtune.h"

#if ((VER_MAJ<<8) + VER_MIN) != BASSVERSION
    #define STRING2(x) #x
    #define STRING(x) STRING2(x)
    #pragma message "BASS version: " STRING(BASSVERSION)
    #pragma message "Plugin version: " STRING(VER_MAJ) "." STRING(VER_MIN) ".*"
    #error "You must set the plugin version (VER_MAJ, VER_MIN) to match BASSVERSION"
#endif

#ifndef bassfunc
    const BASS_FUNCTIONS *bassfunc = nullptr; // may be declared as extern in bass-addon.h
#endif

namespace App {
namespace Plugin {

    //
    // COMMON DECLARATIONS
    //

    static const DWORD HSYNC_MASK   = 0x00FFFFFF;
    static const DWORD POS_KEY_MASK = 0x00FF0000;
    static const DWORD POS_VAL_MASK = 0x0000FFFF;
    static const DWORD TAG_KEY_MASK = 0xFFFF0000;
    static const DWORD TAG_VAL_MASK = 0x0000FFFF;

    static const int BYTES_PER_16BIT_SAMPLE = 4;
    static const int BYTES_PER_FLOAT_SAMPLE = 8;

    static DWORD CALLBACK StreamProc16(HSTREAM handle, void *buffer, DWORD length, void *user);
    static DWORD CALLBACK StreamProcFloat(HSTREAM handle, void *buffer, DWORD length, void *user);
    static void WINAPI Free(void *inst);
    static QWORD WINAPI GetLength(void *inst, DWORD mode);
    static void WINAPI GetInfo(void *inst, BASS_CHANNELINFO *info);
    static BOOL WINAPI CanSetPosition(void *inst, QWORD pos, DWORD mode);
    static QWORD WINAPI SetPosition(void *inst, QWORD pos, DWORD mode);
    static const char* WINAPI GetTags(void *inst, DWORD tags);
    static HSYNC WINAPI SetSync(void *inst, DWORD type, QWORD param, SYNCPROC *proc, void *user);
    static void WINAPI RemoveSync(void *inst, HSYNC sync);

    // add-on function table
    static ADDON_FUNCTIONS funcs = {
        0, // flags - this plugin doesn't support any of additional ADDON_xxx flags
        &Free, // void (WINAPI *Free)(void *inst);
        GetLength, // QWORD (WINAPI *GetLength)(void *inst, DWORD mode);
        GetTags, // const char *WINAPI GetTags(void *inst, DWORD tags);
        nullptr, // GetFilePosition - let BASS handle file position
        GetInfo, // void (WINAPI *GetInfo)(void *inst, BASS_CHANNELINFO *info);
        CanSetPosition, // BOOL (WINAPI *CanSetPosition)(void *inst, QWORD pos, DWORD mode);
        SetPosition, // QWORD (WINAPI *SetPosition)(void *inst, QWORD pos, DWORD mode);
        nullptr, // GetPosition - let BASS handle the position/looping/syncing (POS/END)
        SetSync, // HSYNC WINAPI SetSync(void *inst, DWORD type, QWORD param, SYNCPROC *proc, void *user);
        RemoveSync, // void WINAPI RemoveSync(void *inst, HSYNC sync);
        nullptr, // CanResume - let BASS decide when to resume a stalled stream
        nullptr, // SetFlags - this plugin has no custom flags
        nullptr, // Attribute - this plugin has no attributes
        nullptr // AttributeEx - this plugin has no attributes
    };



    //
    // MODULE AND STREAM CLASSES
    //
    class Module : public ModuleBase
    {
        protected:
            QWORD lenRatio;

            void addTag(const char* key, const std::string& val)
            {
                if(!val.empty())
                {
                    tags.append(key);
                    tags.push_back('=');
                    tags.append(val);
                    tags.push_back('\0');
                }
            }

            void addTag(const char* key, int val)
            {
                if(val)
                {
                    std::stringstream stream;
                    stream << val;
                    std::string strVal = std::string(stream.str());
                    addTag(key, strVal);
                }
            }

            virtual void prepareTags()
            {
                addTag("TYPE", info.type);
                addTag("TITLE", info.title);
                addTag("ARTIST", info.author);
                addTag("PROGRAM", info.program);
                addTag("COMPUTER", info.computer);
                addTag("DATE", info.date);
                addTag("DESCRIPTION", info.comment);
                addTag("VERSION", info.version);
                addTag("STRINGS", info.strings);

                addTag("POSITIONS", info.nPositions);
                addTag("LOOPPOSITION", info.loopPosition);
                addTag("FRAMES", info.nFrames);
                addTag("LOOPFRAME", info.loopFrame);
                addTag("CHANNELS", info.nChannels);
                addTag("INITIALTEMPO", info.initialTempo);
            }

        public:
            Module(::Module::Holder::Ptr moduleHandle, QWORD lenRatio):
                ModuleBase(moduleHandle),
                lenRatio(lenRatio)
            {
            }

            QWORD getPosByteLength()
            {
                return getLength() * lenRatio;
            }
    };

    class Stream : public StreamBase
    {
        protected:
            BASSFILE file;
            DWORD flags;
            HSTREAM handle;
            std::vector<HSYNC> syncs;
            QWORD lenRatio;

            virtual void onTuneChange()
            {
                QWORD pos = bassfunc->GetCount(handle, FALSE);
                for(auto i = syncs.begin(); i < syncs.end(); i++)
                    bassfunc->TriggerSync(handle, *i, pos, 0);
            }

            virtual ModuleBase* createModule(::Module::Holder::Ptr handle)
            {
                return new Module(handle, lenRatio);
            }

            virtual QWORD getFileSize()
            {
                return bassfunc->file.GetPos(file, BASS_FILEPOS_END);
            }

            virtual QWORD fileRead(char* buffer, QWORD length)
            {
                return bassfunc->file.Read(file, buffer, static_cast<DWORD>(length));
            }

        public:
            static const DWORD tagsSyncType = BASS_SYNC_OGG_CHANGE;

            inline HSTREAM getBassHandle() const {return handle;}

            Stream(BASSFILE file, DWORD flags): StreamBase(),
                file(file),
                flags(flags),
                handle(0),
                lenRatio(0)
            {
            }

            virtual bool init()
            {
                // ensure that the file is a local file (with some assumptions)
                DWORD fileflags = bassfunc->file.GetFlags(file);
                if(fileflags & BASSFILE_BUFFERED)
                    error(BASS_ERROR_FILEFORM); // buffered streams are not allowed
                if(flags & BASS_STREAM_BLOCK)
                    error(BASS_ERROR_FILEFORM); // streaming in blocks is not allowed
                if(bassfunc->file.GetPos(file, BASS_FILEPOS_CONNECTED) == 1)
                    error(BASS_ERROR_FILEFORM); // Internet-stream is binded to this channel
                // ensure that DEST/SRC BPS ratios are integer
                static_assert(
                    BYTES_PER_FLOAT_SAMPLE == BYTES_PER_FLOAT_SAMPLE / BYTES_PER_SAMPLE * BYTES_PER_SAMPLE,
                    "BYTES_PER_FLOAT_SAMPLE == BYTES_PER_FLOAT_SAMPLE / BYTES_PER_SAMPLE * BYTES_PER_SAMPLE");
                static_assert(
                    BYTES_PER_16BIT_SAMPLE == BYTES_PER_16BIT_SAMPLE / BYTES_PER_SAMPLE * BYTES_PER_SAMPLE,
                    "BYTES_PER_16BIT_SAMPLE == BYTES_PER_16BIT_SAMPLE / BYTES_PER_SAMPLE * BYTES_PER_SAMPLE");
                // calculate sample ratio between output data and ZXTune-decoded data
                lenRatio = (flags & BASS_SAMPLE_FLOAT)
                           ? BYTES_PER_FLOAT_SAMPLE / BYTES_PER_SAMPLE
                           : BYTES_PER_16BIT_SAMPLE / BYTES_PER_16BIT_SAMPLE;
                if(!StreamBase::init())
                    error(BASS_ERROR_FILEFORM); // something went wrong
                // create a stream
                flags &= // all flags that make sense for this plugin
                        0
                        |BASS_SAMPLE_SOFTWARE
                        |BASS_SAMPLE_3D
                        |BASS_SAMPLE_FX
                        |BASS_STREAM_DECODE
                        |BASS_STREAM_AUTOFREE
                        |BASS_SAMPLE_FLOAT
                        |0x3f000000 // all speaker flags
                        ;
                handle = bassfunc->CreateStream(
                            frequency,
                            Sound::Sample::CHANNELS,
                            flags,
                            (flags & BASS_SAMPLE_FLOAT) ? &StreamProcFloat : &StreamProc16,
                            this,
                            &funcs);
                if(!handle)
                    return false; // some internal error
                if(!bassfunc->file.SetStream(file, handle))
                    error(BASS_ERROR_UNKNOWN); // some weird internal error
                return true;
            }

            QWORD render16(void *buffer, QWORD length)
            {
                static_assert(
                    BYTES_PER_16BIT_SAMPLE == BYTES_PER_SAMPLE,
                    "BYTES_PER_16BIT_SAMPLE == BYTES_PER_SAMPLE");
                bool atEnd;
                QWORD result = render(buffer, length, atEnd);
                if(atEnd)
                    result |= BASS_STREAMPROC_END;
                return result;
            }

            QWORD renderFloat(void *buffer, QWORD length)
            {
                bool atEnd;
                QWORD ratio = BYTES_PER_FLOAT_SAMPLE / BYTES_PER_SAMPLE;
                length = render(buffer, length/ratio, atEnd); // can only decode length/ratio bytes
                DWORD nNumbers = length / Sound::Sample::CHANNELS; // each sample has this many numbers to convert
                bassfunc->data.Int2Float(buffer, static_cast<float*>(buffer), nNumbers, 2);
                length = length * ratio; // resulting float samples buffer length
                if(atEnd)
                    length |= BASS_STREAMPROC_END;
                return length;
            }

            QWORD getPosByteLength()
            {
                return getLength() * lenRatio;
            }

            QWORD seekPosByte(QWORD pos)
            {
                return seek(pos / lenRatio) * lenRatio;
            }

            HSYNC addTagsSync(SYNCPROC *proc, void *user)
            {
                HSYNC sync = bassfunc->NewSync(handle, tagsSyncType, proc, user);
                if(sync)
                    syncs.push_back(sync);
                return sync;
            }

            void removeSync(HSYNC sync)
            {
                for(auto i = syncs.begin(); i < syncs.end(); i++)
                {
                    if(*i == sync)
                    {
                        syncs.erase(i);
                        break;
                    }
                }
            }
    };



    //
    // INTERFACE FUNCTIONS
    //

    static BOOL CALLBACK ConfigProc(DWORD option, DWORD flags, void *value)
    {
        if(!(flags & BASSCONFIG_PTR))
        {
            DWORD *dvalue = static_cast<DWORD*>(value);
            switch(option)
            {
                case BASS_CONFIG_ZXTUNE_MAXFILESIZE:
                    if(flags & BASSCONFIG_SET)
                        MAX_FILESIZE = *dvalue;
                    else
                        *dvalue = MAX_FILESIZE;
                    return TRUE;
            }
        }
        return FALSE;
    }

    void WINAPI Free(void *inst)
    {
        delete static_cast<Stream*>(inst);
    }

    // called by BASS_StreamGetLength - get the stream playback length
    static QWORD WINAPI GetLength(void *inst, DWORD mode)
    {
        Stream *stream = static_cast<Stream*>(inst);
        switch(mode)
        {
            case BASS_POS_BYTE:
                noerrorn(stream->getPosByteLength());

            case BASS_POS_OGG:
            case BASS_POS_ZXTUNE_SUB_COUNT:
                noerrorn(stream->getModules().size());

            default:
                if((mode & POS_KEY_MASK) == BASS_POS_ZXTUNE_SUB_LENGTH)
                {
                    WORD i = mode & POS_VAL_MASK;
                    if(i >= stream->getModules().size())
                        errorn(BASS_ERROR_POSITION);
                    Module* module = static_cast<Module*>(stream->getModules()[i]);
                    noerrorn(module->getPosByteLength());
                }

                errorn(BASS_ERROR_NOTAVAIL); // other modes are not supported
        }
    }

    // called by BASS_ChannelGetInfo
    static void WINAPI GetInfo(void *inst, BASS_CHANNELINFO *info)
    {
        (void)inst;
        info->ctype = BASS_CTYPE_MUSIC_ZXTUNE;
        info->origres = BYTES_PER_SAMPLE / Sound::Sample::CHANNELS * 8; // bits
    }

    // called by BASS_ChannelSetPosition
    // return TRUE if seeking to the requested position is possible, otherwise return
    // FALSE and set the error code
    static BOOL WINAPI CanSetPosition(void *inst, QWORD pos, DWORD mode)
    {
        Stream *stream = static_cast<Stream*>(inst);
        switch(mode & POS_VAL_MASK)
        {
            case BASS_POS_BYTE:
                if(pos > stream->getPosByteLength())
                    error(BASS_ERROR_POSITION);
                return TRUE;

            default:
                error(BASS_ERROR_NOTAVAIL); // other modes are not supported
        }
    }

    // called by BASS_ChannelSetPosition
    // called after the above function - do the actual seeking
    // return the actual resulting position (-1 = error)
    static QWORD WINAPI SetPosition(void *inst, QWORD pos, DWORD mode)
    {
        Stream *stream = static_cast<Stream*>(inst);
        QWORD result;
        switch(mode & POS_VAL_MASK)
        {
            case BASS_POS_BYTE:
                result = stream->seekPosByte(pos);
                if(result == NEG_QWORD)
                    errorn(BASS_ERROR_POSITION);
                return result;

            default:
                errorn(BASS_ERROR_NOTAVAIL); // other modes are not supported
        }
        errorn(BASS_ERROR_NOTAVAIL); // other modes are not supported
    }

    // called by BASS_StreamGetTags - get the stream tags
    static const char* WINAPI GetTags(void *inst, DWORD tags)
    {
        Stream* stream = static_cast<Stream*>(inst);

        if(tags == BASS_TAG_OGG)
        {
            const char* result = stream->getTagsPtr();
            if(!result)
                errorp(BASS_ERROR_NOTAVAIL, const char);
            return result;
        }

        if((tags & TAG_KEY_MASK) == BASS_TAG_ZXTUNE_SUB_OGG)
        {
            WORD i = tags & TAG_VAL_MASK;
            if(i >= stream->getModules().size())
                errorp(BASS_ERROR_POSITION, const char);
            Module* module = static_cast<Module*>(stream->getModules()[i]);
            const char* result = module->getTagsPtr();
            if(!result)
                errorp(BASS_ERROR_NOTAVAIL, const char);
            return result;
        }

        errorp(BASS_ERROR_NOTAVAIL, const char);
    }

    // called by BASS_ChannelSetSync
    // return -1 to let BASS handle the sync (POS/END)
    static HSYNC WINAPI SetSync(void *inst, DWORD type, QWORD param, SYNCPROC *proc, void *user)
    {
        (void)param;

        if((type & HSYNC_MASK) != Stream::tagsSyncType)
            return static_cast<HSYNC>(-1);

        return static_cast<Stream*>(inst)->addTagsSync(proc, user);
    }

    // called when a sync is removed, either by BASS_ChannelRemoveSync or due to ONETIME flag
    static void WINAPI RemoveSync(void *inst, HSYNC sync)
    {
        static_cast<Stream*>(inst)->removeSync(sync);
    }

    // returns 16-bit data
    static DWORD CALLBACK StreamProc16(HSTREAM handle, void *buffer, DWORD length, void *user)
    {
        (void)handle;
        return static_cast<DWORD>(static_cast<Stream*>(user)->render16(buffer, static_cast<QWORD>(length)));
    }

    // returns floating point 32-bit data
    static DWORD CALLBACK StreamProcFloat(HSTREAM handle, void *buffer, DWORD length, void *user)
    {
        (void)handle;
        return static_cast<DWORD>(static_cast<Stream*>(user)->renderFloat(buffer, static_cast<QWORD>(length)));
    }

    static HSTREAM WINAPI StreamCreateProc(BASSFILE file, DWORD flags)
    {
        Stream *stream = new Stream(file, flags);
        if(!stream->init())
        {
            delete stream;
            return 0;
        }
        noerrorn(stream->getBassHandle());
    }

    static bool inited = false;

    static bool init()
    {
        if(!inited)
        {
            // register plugin
            if(HIWORD(BASS_GetVersion()) != BASSVERSION)
                return false;
            if(!GetBassFunc())
                return false;
            bassfunc->RegisterPlugin(reinterpret_cast<void*>(ConfigProc), PLUGIN_CONFIG_ADD); // register config function

            // done
            inited = true;
        }
        return true;
    }

    static void deinit()
    {
        if(inited)
        {
            bassfunc->RegisterPlugin(reinterpret_cast<void*>(ConfigProc), PLUGIN_CONFIG_REMOVE); // unregister config function
            inited = false;
        }
    }

    static const BASS_PLUGININFO* WINAPI getPluginInfo()
    {
        static const BASS_PLUGININFO* result = nullptr;
        if(result)
            return result;

        assert(supportedExtensions.size());

        static std::string line;
        for(const auto& ext : supportedExtensions)
        {
            line.append("*.");
            line.append(ext);
            line.append(";");
        }
        line.pop_back();

        static const std::vector<BASS_PLUGINFORM> pluginForms = {{
            BASS_CTYPE_MUSIC_ZXTUNE,
            "ZXTune-supported module",
            line.data()
        }};

        static const BASS_PLUGININFO pluginInfo = {
            MAKELONG(MAKEWORD(0x0, VER_PAT), MAKEWORD(VER_MIN, VER_MAJ)),
            static_cast<DWORD>(pluginForms.size()),
            pluginForms.data()
        };

        result = &pluginInfo;
        return result;
    }
}}

BASSZXTUNE_API(const void*)
#if defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
    BASS_ZXTUNE_plugin // symbol to use in BASS_PluginLoad call on iPhoneOS
#else
    BASSplugin
#endif
(DWORD face)
{
    if(!App::Plugin::init())
        return nullptr;

    switch (face)
    {
        case BASSPLUGIN_INFO:
            return static_cast<const void*>(App::Plugin::getPluginInfo());

        case BASSPLUGIN_CREATE:
            return reinterpret_cast<const void*>(App::Plugin::StreamCreateProc);
    }

    return nullptr;
}

#ifdef _WIN32
BOOL WINAPI DllMain(HINSTANCE hDLL, DWORD reason, LPVOID reserved)
{
    (void)reserved;

    switch (reason)
    {
        case DLL_PROCESS_ATTACH:
            DisableThreadLibraryCalls(static_cast<HMODULE>(hDLL));
            return App::Plugin::init();

        case DLL_PROCESS_DETACH:
            App::Plugin::deinit();
            return TRUE;
    }

    return TRUE;
}
#else
static void __attribute__ ((constructor)) PROCESS_ATTACH()
{
    App::Plugin::init();
}
static void __attribute__ ((destructor)) PROCESS_DETACH()
{
    App::Plugin::deinit();
}
#endif

BASSZXTUNE_API(HSTREAM) BASS_ZXTUNE_StreamCreateFile(BOOL mem, const void *file, QWORD offset, QWORD length, DWORD flags)
{
    if(!App::Plugin::init())
        error(BASS_ERROR_VERSION);

    BASSFILE bassFile = bassfunc->file.Open(mem, file, offset, length, flags, TRUE);
    if(!bassFile)
        return 0;
    HSTREAM stream = App::Plugin::StreamCreateProc(bassFile, flags);
    if(!stream)
        bassfunc->file.Close(bassFile);

    return stream;
}

BASSZXTUNE_API(HSTREAM) BASS_ZXTUNE_StreamCreateFileUser(DWORD system, DWORD flags, const BASS_FILEPROCS *procs, void *user)
{
    if(!App::Plugin::init())
        error(BASS_ERROR_VERSION);

    BASSFILE bassFile = bassfunc->file.OpenUser(system, flags, procs, user, TRUE);
    if(!bassFile)
        return 0;
    HSTREAM stream = App::Plugin::StreamCreateProc(bassFile, flags);
    if(!stream)
        bassfunc->file.Close(bassFile);

    return stream;
}
