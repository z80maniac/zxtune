/*************************************************************************}
{ pluginbase.cpp - common functionality for all plugins                   }
{                                                                         }
{ (c) Alexey Parfenov, 2015                                               }
{                                                                         }
{ e-mail: zxed@alkatrazstudio.net                                         }
{                                                                         }
{ This program is free software; you can redistribute it and/or           }
{ modify it under the terms of the GNU General Public License             }
{ as published by the Free Software Foundation; either version 3 of       }
{ the License, or (at your option) any later version.                     }
{                                                                         }
{ This program is distributed in the hope that it will be useful,         }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of          }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        }
{ General Public License for more details.                                }
{                                                                         }
{ You may read GNU General Public License at:                             }
{   http://www.gnu.org/copyleft/gpl.html                                  }
{*************************************************************************/

#include "pluginbase.h"

#include <make_ptr.h>
#include <error_tools.h>

#include <binary/container_factories.h>
#include <core/module_detect.h>
#include <module/attributes.h>
#include <module/track_information.h>

#include <boost/bind.hpp>
#include <boost/range/end.hpp>

namespace App {
namespace Plugin {

    static DWORD DEFAULT_MAX_SIZE_MB =
#ifdef MAXFILESIZE
            MAXFILESIZE;
#else
            256;
#endif

    // set default max file size to prevent memory hogging
    DWORD MAX_FILESIZE = DEFAULT_MAX_SIZE_MB * 1024 * 1024;

    static const std::size_t RENDERER_BUFFER_SIZE = 32768;

    //
    // MODULE
    //

    ModuleBase::ModuleBase(const Module::Holder::Ptr moduleHandle):
        handle(moduleHandle),
        nBytes(0),
        nSamples(0),
        nSecs(0),
        frequency(0),
        frameDuration(0),
        tagsReady(false),
        tagsPtr(nullptr),
        position(0),
        samplePosition(0),
        startPos(0),
        endPos(0)
    {
    }

    ModuleBase::~ModuleBase()
    {
        destroyPlayer();
        handle = nullptr;
    }

    void ModuleBase::setMaxQuality()
    {
        player->setParam(
            Parameters::ZXTune::Core::SAA::INTERPOLATION,
            Parameters::ZXTune::Core::SAA::INTERPOLATION_HQ);
        player->setParam(
            Parameters::ZXTune::Core::AYM::INTERPOLATION,
            Parameters::ZXTune::Core::AYM::INTERPOLATION_HQ);
        player->setParam(
            Parameters::ZXTune::Core::SID::INTERPOLATION,
            Parameters::ZXTune::Core::SID::INTERPOLATION_HQ);
        player->setParam(
            Parameters::ZXTune::Core::SID::FILTER,
            Parameters::ZXTune::Core::SID::FILTER_ENABLED);
        player->setParam(
            Parameters::ZXTune::Core::DAC::INTERPOLATION,
            Parameters::ZXTune::Core::DAC::INTERPOLATION_YES);
    }

    void ModuleBase::setMinQuality()
    {
        player->setParam(
            Parameters::ZXTune::Core::SAA::INTERPOLATION,
            Parameters::ZXTune::Core::SAA::INTERPOLATION_NONE);
        player->setParam(
            Parameters::ZXTune::Core::AYM::INTERPOLATION,
            Parameters::ZXTune::Core::AYM::INTERPOLATION_NONE);
        player->setParam(
            Parameters::ZXTune::Core::SID::INTERPOLATION,
            Parameters::ZXTune::Core::SID::INTERPOLATION_NONE);
        player->setParam(
            Parameters::ZXTune::Core::SID::FILTER,
            Parameters::ZXTune::Core::SID::FILTER_DISABLED);
        player->setParam(
            Parameters::ZXTune::Core::DAC::INTERPOLATION,
            Parameters::ZXTune::Core::DAC::INTERPOLATION_NO);
    }

    bool ModuleBase::fillMainInfo()
    {
        try
        {
            const Module::Information::Ptr modInfo = handle->GetModuleInformation();
            const Module::TrackInformation::Ptr trackInfo =
                    std::dynamic_pointer_cast<const Module::TrackInformation>(modInfo);
            if(trackInfo)
            {
                info.nPositions = trackInfo->PositionsCount();
                info.loopPosition = trackInfo->LoopPosition();
                info.nFrames = modInfo->FramesCount();
                info.loopFrame = modInfo->LoopFrame();
                info.nChannels = modInfo->ChannelsCount();
                info.initialTempo = trackInfo->Tempo();
            }
            else
            {
                info.nPositions = 0;
                info.loopPosition = 0;
                info.nFrames = modInfo->FramesCount();
                info.loopFrame = modInfo->LoopFrame();
                info.nChannels = modInfo->ChannelsCount();
                info.initialTempo = 0;
            }
            return true;
        }
        catch (const Error&){}
        catch (const std::exception&){}

        info.nPositions = 0;
        info.loopPosition = 0;
        info.nFrames = 0;
        info.loopFrame = 0;
        info.nChannels = 0;
        info.initialTempo = 0;

        return false;
    }

    bool ModuleBase::fillPropsInfo()
    {
        try
        {
            const Parameters::Accessor::Ptr modProps = handle->GetModuleProperties();

            if(!modProps->FindValue(Module::ATTR_TYPE, info.type))
                info.type = "";
            if(!modProps->FindValue(Module::ATTR_TITLE, info.title))
                info.title = "";
            if(!modProps->FindValue(Module::ATTR_AUTHOR, info.author))
                info.author = "";
            if(!modProps->FindValue(Module::ATTR_PROGRAM, info.program))
                info.program = "";
            if(!modProps->FindValue(Module::ATTR_COMPUTER, info.computer))
                info.computer = "";
            if(!modProps->FindValue(Module::ATTR_DATE, info.date))
                info.date = "";
            if(!modProps->FindValue(Module::ATTR_COMMENT, info.comment))
                info.comment = "";
            if(!modProps->FindValue(Module::ATTR_VERSION, info.version))
                info.version = "";
            if(!modProps->FindValue(Module::ATTR_VERSION, info.versionInt))
                info.versionInt = 0;
            if(!modProps->FindValue(Module::ATTR_STRINGS, info.strings))
                info.strings = "";

            if(info.version.empty() && info.versionInt)
            {
                std::stringstream stream;
                stream << info.versionInt;
                info.version = std::string(stream.str());
            }

            return true;
        }
        catch (const Error&){}
        catch (const std::exception&){}

        info.type = "";
        info.title = "";
        info.author = "";
        info.program = "";
        info.computer = "";
        info.date = "";
        info.comment = "";
        info.version = "";
        info.versionInt = 0;
        info.strings = "";

        return false;
    }

    bool ModuleBase::init(int _frequency)
    {
        static_assert(
            Sound::Sample::CHANNELS == 2,
            "Sound::Sample::CHANNELS == 2"); // support only 2 channels

        // initialize ZXTune player
        Player::Ptr _player = createPlayerRaw();
        if(!_player)
            return false; // ZXTune_CreatePlayer failed, some internal exception in ZXTune library
        if(!fillMainInfo())
            return false; // can't get any information about loaded module

        // set frequency
        frequency = _frequency;
        _player->setParam(Parameters::ZXTune::Sound::FREQUENCY, frequency);
        if(!_player->getParam(Parameters::ZXTune::Sound::FREQUENCY, &frequency))
            return false; // cannot get a real frequency
        if(frequency <= 0)
            return false; // invalid frequency

        // calculate the total file length in bytes for a resulting pseudo-stream
        if(!_player->getParam(Parameters::ZXTune::Sound::FRAMEDURATION, &frameDuration))
            return false; // cannot get a frame duration
        if(frameDuration <= 0)
            return false; // invalid frame duration
        double allFramesDuration = info.nFrames * frameDuration;
        nSecs = allFramesDuration / FRAMERATE_MULTIPLIER;
        nSamples = static_cast<QWORD>(nSecs * frequency);
        nBytes = nSamples * BYTES_PER_SAMPLE;

        // keep the player
        player = _player;

        // fill tags data
        // does not matter if it fails
        fillPropsInfo();

        // notify the subclass that the tags are ready
        prepareTags();
        if(tags.empty())
            tagsPtr = nullptr;
        else
            tagsPtr = tags.c_str();

        return true;
    }

    void ModuleBase::initPlayer()
    {
        // set parameters, ignore all errors
        player->setParam(Parameters::ZXTune::Sound::FREQUENCY, frequency);
        setMaxQuality();
    }

    QWORD ModuleBase::seek(QWORD pos)
    {
        QWORD sample = pos / BYTES_PER_SAMPLE;

        // seeking includes decoding so switch to lo-fi first to speed things up
        setMinQuality();
        samplePosition = player->seek(sample);
        setMaxQuality();

        position = samplePosition * BYTES_PER_SAMPLE;
        return position;
    }

    void ModuleBase::destroyPlayer()
    {
        player = nullptr;
    }

    Player::Ptr ModuleBase::createPlayerRaw()
    {
        try{
            return std::make_shared<Player>(handle);
        }catch(...){
            return nullptr;
        }
    }

    Player::Ptr ModuleBase::createPlayer()
    {
        if(player)
            return player;

        player = createPlayerRaw(); // if it fails, then the playback should be stalled
        if(!player)
            return nullptr;
        initPlayer();
        return player;
    }

    QWORD ModuleBase::renderSamples(Sound::Sample *buffer, QWORD count)
    {
        count = std::min(count, nSamples - samplePosition);
        if(count <= 0)
            return 0;
        count = player->render(buffer, count);
        if(count <= 0)
            return 0;
        samplePosition += count;
        position += count * BYTES_PER_SAMPLE;
        return count;
    }

    QWORD ModuleBase::render(void *buffer, QWORD length)
    {
        if(length < BYTES_PER_SAMPLE)
            return 0; // the length is too small

        QWORD nSamples = length / BYTES_PER_SAMPLE; // get a number of full samples
        nSamples = renderSamples(static_cast<Sound::Sample*>(buffer), nSamples);
        return nSamples * BYTES_PER_SAMPLE;
    }

    //
    // STREAM
    //

    ModuleBase* StreamBase::createModule(Module::Holder::Ptr handle)
    {
        return new ModuleBase(handle);
    }

    ModuleBase* StreamBase::addModule(Module::Holder::Ptr handle)
    {
        ModuleBase* module = createModule(handle);

        if(frequency)
        {
            if(!module->init(frequency))
            {
                delete module;
                return nullptr;
            }
            module->destroyPlayer(); // don't need it right now

            if(module->getFrequency() != frequency)
            {
                delete module;
                return nullptr; // this module has different frequency, it can't be played corectly
            }
        }
        else
        {
            if(!module->init())
            {
                delete module;
                return nullptr;
            }

            frequency = module->getFrequency(); // all other modules must have the same frequency
        }

        module->startPos = nBytes;
        nBytes += module->getLength();
        nSecs += module->getDuration();
        module->endPos = nBytes;
        modules.push_back(module);

        return module;
    }

    bool StreamBase::switchModule()
    {
        ModuleBase* newModule = getCurModule();
        if(oldModule)
            oldModule->destroyPlayer();
        oldModule = newModule;
        if(!newModule->createPlayer())
            return false;
        return true;
    }

    QWORD StreamBase::getLength() const
    {
        if(singleModuleMode)
            return getCurModule()->getLength();
        else
            return nBytes;
    }

    double StreamBase::getDuration() const
    {
        if(singleModuleMode)
            return getCurModule()->getDuration();
        else
            return nSecs;
    }

    QWORD StreamBase::getPosition() const
    {
        if(singleModuleMode)
            return getCurModule()->getPosition();
        else
            return getCurModule()->getPosition() + getCurModule()->startPos;
    }

    bool StreamBase::jumpToModule(size_t index)
    {
        if(index < modules.size())
        {
            Modules::iterator i = modules.begin() + static_cast<int>(index);
            if(i != curModuleIter)
            {
                curModuleIter = i;
                switchModule();
            }
            return getCurModule()->seek(0) == 0;
        }
        return false;
    }

    StreamBase::StreamBase():
        data(nullptr),
        nBytes(0),
        nSecs(0),
        frequency(0),
        oldModule(nullptr),
        singleModuleMode(false)
    {
    }

    StreamBase::~StreamBase()
    {
        for(auto i = modules.begin(); i < modules.end(); i++)
            delete *i;
    }

    std::vector<Module::Holder::Ptr> StreamBase::detectModules(const Binary::Container::Ptr data)
    {
        std::vector<Module::Holder::Ptr> modules;

        struct ModuleDetector : public Module::DetectCallback
        {
            std::vector<Module::Holder::Ptr>* modules;
            ModuleDetector(std::vector<Module::Holder::Ptr>* mods) : modules(mods) {}

            virtual void ProcessModule(ZXTune::DataLocation::Ptr, ZXTune::Plugin::Ptr, Module::Holder::Ptr holder) const
            {
                modules->push_back(holder);
            }

            virtual Log::ProgressCallback* GetProgress() const {return NULL;}
        };

        try
        {
            Module::Detect(*Parameters::Container::Create(), data, ModuleDetector(&modules));
        }
        catch (const Error&){}
        catch (const std::exception&){}

        return modules;
    }

    bool StreamBase::init()
    {
        QWORD fileLen = getFileSize();

        // check file length
        if(fileLen > MAX_FILESIZE)
            return false; // file is too big

        // make a memory buffer for the file
        char* buf;
        try{
            buf = new char[fileLen];
        }catch(...){
            return false; // cannot allocate a buffer for a file
        }

        // read the whole file into memory
        char* p = buf;
        QWORD bytesLeft = fileLen;
        while(bytesLeft)
        {
            QWORD n = fileRead(p, bytesLeft);
            if(!n)
            {
                delete[] buf;
                return false; // not at the end, but no bytes available either
            }
            if(n == NEG_QWORD)
            {
                delete[] buf;
                return false; // some internal error while reading the file
            }

            bytesLeft = bytesLeft - n;
            if(!bytesLeft)
                break;
        }

        // load file data into libzxtune
        data = Binary::CreateContainer(buf, fileLen);
        delete[] buf; // don't need the buffer anymore
                      // since there's a copy of it in the container
        if(!data)
            return false;

        // find modules in the data
        std::vector<Module::Holder::Ptr> handles = detectModules(data);
        for(auto i = handles.begin(); i < handles.end(); i++)
            addModule(*i);
        if(modules.empty())
        {
            data = nullptr;
            return false; // no valid modules found
        }

        // first-time initialization
        curModuleIter = modules.begin();
        lastModuleIter = modules.end();
        lastModuleIter--;
        oldModule = getCurModule();
        oldModule->initPlayer();

        return true;
    }

    QWORD StreamBase::seek(QWORD pos)
    {
        if(singleModuleMode)
            return getCurModule()->seek(pos);

        for(auto i = modules.begin(); i < modules.end(); i++)
        {
            ModuleBase* module = *i;
            if(pos < module->endPos)
            {
                if(curModuleIter == i)
                {
                    QWORD startPos = getCurModule()->startPos;
                    return startPos + getCurModule()->seek(pos - startPos);
                }
                else
                {
                    curModuleIter = i;
                    switchModule();
                    QWORD startPos = getCurModule()->startPos;
                    QWORD result = startPos + getCurModule()->seek(pos - startPos);
                    onTuneChange();
                    return result;
                }
            }
        }

        return NEG_QWORD;
    }

    QWORD StreamBase::render(void *buffer, QWORD length, bool &atEnd)
    {
        QWORD nSamples = length / BYTES_PER_SAMPLE;
        return BYTES_PER_SAMPLE * renderSamples(static_cast<Sound::Sample*>(buffer), nSamples, atEnd);
    }

    QWORD StreamBase::render(void *buffer, QWORD length)
    {
        bool atEnd;
        return render(buffer, length, atEnd);
    }

    QWORD StreamBase::renderSamples(Sound::Sample *buffer, QWORD nSamples, bool &atEnd)
    {
        QWORD nTotal = 0;

        if(nSamples)
        {
            while(true)
            {
                QWORD samplesDone = getCurModule()->renderSamples(buffer, nSamples);
                if(!samplesDone)
                {
                    if(singleModuleMode)
                    {
                        atEnd = true;
                        break;
                    }
                    if(curModuleIter == lastModuleIter)
                    {
                        atEnd = true; // currently at the end of the last module
                        break;
                    }

                    // jump to the beginning of next module
                    ++curModuleIter;
                    switchModule();
                    getCurModule()->seek(0);
                    onTuneChange();
                }
                else
                {
                    nTotal += samplesDone;
                    buffer += samplesDone;
                    nSamples -= samplesDone;
                    if(!nSamples)
                    {
                        atEnd = false;
                        break;
                    }
                }
            }
        }

        return nTotal;
    }

    QWORD StreamBase::renderSamples(Sound::Sample *buffer, QWORD nSamples)
    {
        bool atEnd;
        return renderSamples(buffer, nSamples, atEnd);
    }

    //
    // PLAYER
    //

    Player::Player(const Module::Holder::Ptr moduleHandle)
        : params(Parameters::Container::Create())
        , buffer(MakePtr<Renderer>())
        , renderer(moduleHandle->CreateRenderer(params, buffer))
    {
        //copy initial properties
        moduleHandle->GetModuleProperties()->Process(*params);
        renderer->Reset();
    }

    std::size_t Player::render(Sound::Sample *target, std::size_t samples)
    {
        std::size_t result = 0;
        bool hasNextFrame = true;
        while (samples)
        {
            std::size_t got = buffer->getSamples(samples, target);
            target += got;
            samples -= got;
            result += got;
            if(0 == samples)
                break;
            if(!hasNextFrame)
                break;
            hasNextFrame = renderer->RenderFrame();
        }
        return result;
    }

    std::size_t Player::seek(std::size_t samples)
    {
        if(samples < buffer->getCurrentSample())
            reset();

        bool hasNextFrame = true;
        while(samples != buffer->getCurrentSample())
        {
            const std::size_t toDrop = samples - buffer->getCurrentSample();
            if(buffer->dropSamples(toDrop))
                continue;
            if(!hasNextFrame)
                break;
            hasNextFrame = renderer->RenderFrame();
        }

        return buffer->getCurrentSample();
    }

    void Player::reset()
    {
        renderer->Reset();
        buffer->reset();
    }

    void Player::setParam(const Parameters::NameType &paramName, int paramValue)
    {
        params->SetValue(paramName, paramValue);
    }

    bool Player::findDefaultValue(const Parameters::NameType &name, Parameters::IntType &value)
    {
        typedef std::pair<Parameters::NameType, Parameters::IntType> Name2Val;
        static const Name2Val DEFAULTS[] =
        {
            Name2Val(Parameters::ZXTune::Sound::FREQUENCY, Parameters::ZXTune::Sound::FREQUENCY_DEFAULT),
            Name2Val(Parameters::ZXTune::Core::AYM::CLOCKRATE, Parameters::ZXTune::Core::AYM::CLOCKRATE_DEFAULT),
            Name2Val(Parameters::ZXTune::Sound::FRAMEDURATION, Parameters::ZXTune::Sound::FRAMEDURATION_DEFAULT),
        };

        const Name2Val* const defVal = std::find_if(DEFAULTS, boost::end(DEFAULTS), boost::bind(&Name2Val::first, _1) == name);
        if (boost::end(DEFAULTS) == defVal)
            return false;

        value = defVal->second;
        return true;
    }

    bool Player::getParam(const Parameters::NameType &paramName, int *paramValue)
    {
        Parameters::IntType value;
        if(!params->FindValue(paramName, value) && !findDefaultValue(paramName, value))
            return false;
        *paramValue = static_cast<int>(value);
        return true;
    }

    //
    // RENDERER
    //

    Renderer::Renderer() : Sound::Receiver()
      , buffer(RENDERER_BUFFER_SIZE)
      , doneSamples()
    {
    }

    void Renderer::ApplyData(Sound::Chunk data)
    {
        buffer.Put(data.begin(), data.size());
    }

    std::size_t Renderer::getSamples(std::size_t count, Sound::Sample *target)
    {
        const Sound::Sample* part1 = 0;
        std::size_t part1Size = 0;
        const Sound::Sample* part2 = 0;
        std::size_t part2Size = 0;
        if (const std::size_t toGet = buffer.Peek(count, part1, part1Size, part2, part2Size))
        {
            std::memcpy(target, part1, part1Size * sizeof(*part1));
            if (part2)
            {
                std::memcpy(
                            target + part1Size,
                            part2,
                            part2Size * sizeof(*part2));
            }
            buffer.Consume(toGet);
            doneSamples += toGet;
            return toGet;
        }
        return 0;
    }

    std::size_t Renderer::dropSamples(std::size_t count)
    {
        const std::size_t toDrop = buffer.Consume(count);
        doneSamples += toDrop;
        return toDrop;
    }

    void Renderer::reset()
    {
        buffer.Reset();
        doneSamples = 0;
    }

}}
