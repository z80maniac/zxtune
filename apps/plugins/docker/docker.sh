#!/usr/bin/env bash
set -e
cd "$(dirname -- "$(readlink -f -- "$0")")"

function err
{
    echo "$1" 1>&2
    exit 1
}

function usage
{
    err "
ERROR: $1

Usage: $0 <distribution> <command> <target> [args...]

distribution:
    - linux-x86_64
    - android-arm_v7a
    - android-arm64_v8a
    - android-i686
    - android-x86_64

command:
    - make
    - run

make target:
    - bass
    - bass/test
    - bass/libtest
    - xmplay
"
}

DIST="$1"
if [[ -z $DIST ]]
then
    usage "distribution not specified"
fi
shift

COMMAND="$1"
if [[ -z $COMMAND ]]
then
    usage "command not specified"
fi
shift

TARGET="$1"
if [[ -z $TARGET ]]
then
    usage "target not specified"
fi
shift

ARGS=("$@")
DOCKER_ARGS=()

case "$DIST" in
    linux-x86_64)
        DOCKER_SUFFIX=linux
        DOCKER_ARGS=(-e ARCH=x86_64 -e LD_LIBRARY_PATH=bin/linux_x86_64/release)
        ;;

    android-arm_v7a)
        DOCKER_SUFFIX=android
        DOCKER_ARGS=(-e PLATFORM=android -e NDK_TOOLCHAIN=/toolchain -e ARCH=arm_v7a)
        ;;

    android-arm64_v8a)
        DOCKER_SUFFIX=android
        DOCKER_ARGS=(-e PLATFORM=android -e NDK_TOOLCHAIN=/toolchain -e ARCH=arm64_v8a)
        ;;

    android-i686)
        DOCKER_SUFFIX=android
        DOCKER_ARGS=(-e PLATFORM=android -e NDK_TOOLCHAIN=/toolchain -e ARCH=i686)
        ;;

    android-x86_64)
        DOCKER_SUFFIX=android
        DOCKER_ARGS=(-e PLATFORM=android -e NDK_TOOLCHAIN=/toolchain -e ARCH=x86_64)
        ;;

    *)
        usage "unknown distribution: $DIST"
        ;;
esac

DOCKER_FILE=Dockerfile.$DOCKER_SUFFIX
DOCKER_IMAGE=zxtune-plugins-build-$DOCKER_SUFFIX
DOCKER_UID="$(id -u)"
DOCKER_GID="$(id -g)"
SRC_DIR="$(readlink -e -- "$(pwd)/../../..")"
DOCKER_ALL_ARGS=(
    --rm -it --init
    -u"$DOCKER_UID:$DOCKER_GID"
    -v "$SRC_DIR":/zxtune -w /zxtune
    "${DOCKER_ARGS[@]}"
    $DOCKER_IMAGE
)

sudo docker build -t $DOCKER_IMAGE -f $DOCKER_FILE . > /dev/null

case "$COMMAND" in
    make)
        sudo docker run "${DOCKER_ALL_ARGS[@]}" apps/plugins/build.sh $TARGET "${ARGS[@]}"
        ;;

    run)
        sudo docker run "${DOCKER_ALL_ARGS[@]}" $TARGET $ARGS
        ;;

    *)
        usage "unknown command: $COMMAND"
        ;;
esac
