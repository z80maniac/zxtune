/*************************************************************************}
{ xmpzxtune.cpp - XMPlay plugin                                           }
{                                                                         }
{ (c) Alexey Parfenov, 2015                                               }
{                                                                         }
{ e-mail: zxed@alkatrazstudio.net                                         }
{                                                                         }
{ This program is free software; you can redistribute it and/or           }
{ modify it under the terms of the GNU General Public License             }
{ as published by the Free Software Foundation; either version 3 of       }
{ the License, or (at your option) any later version.                     }
{                                                                         }
{ This program is distributed in the hope that it will be useful,         }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of          }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        }
{ General Public License for more details.                                }
{                                                                         }
{ You may read GNU General Public License at:                             }
{   http://www.gnu.org/copyleft/gpl.html                                  }
{*************************************************************************/

#include "../common/pluginbase.h"
#include "res/configdialog.h"
#include "xmpin.h"
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/split.hpp>
#include <algorithm>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <mutex>
#include <map>

#define AS_STR2(x) #x
#define AS_STR(x) AS_STR2(x)

#if XMPIN_FACE != 4
    #error not compatible with the current XMPIN_FACE
#endif

namespace App {
namespace Plugin {

    //
    // XMP INTERFACES
    //

    static XMPFUNC_IN *xmpfin;
    static XMPFUNC_MISC *xmpfmisc;
    static XMPFUNC_FILE *xmpffile;
    static XMPFUNC_TEXT *xmpftext;
    static XMPFUNC_REGISTRY *xmpfregistry;
    static DWORD xmpver;



    //
    // MODULE AND STREAM CLASSES
    //

    class Module : public ModuleBase
    {
        protected:
            virtual void addTag(const char* key, const std::string& val)
            {
                if(!val.empty())
                {
                    tags.append(key);
                    tags.push_back('\0');
                    tags.append(val);
                    tags.push_back('\0');
                }
            }

            virtual void prepareTags()
            {
                addTag("filetype", info.type);
                addTag("title", info.title);
                addTag("artist", info.author);
                addTag("date", info.date);
                addTag("comment", info.comment);
            }

        public:
            Module(::Module::Holder::Ptr moduleHandle): ModuleBase(moduleHandle) {}
    };

    class Stream : public StreamBase
    {
        protected:
            XMPFILE file;
            Sound::Sample::Type sampleBuf[Sound::Sample::CHANNELS];
            unsigned int curChannel;
            static constexpr float conversionScale = 1.f / (1 << (Sound::Sample::BITS-1));
            int anchorModuleIndex;

            virtual ModuleBase* createModule(::Module::Holder::Ptr handle)
            {
                return new Module(handle);
            }

            virtual QWORD getFileSize()
            {
                return xmpffile->GetSize(file);
            }

            virtual QWORD fileRead(char* buffer, QWORD length)
            {
                return xmpffile->Read(file, buffer, length);
            }

            virtual void onTuneChange()
            {
                xmpfin->UpdateTitle(nullptr);
            }

            bool getFloat(float *buffer)
            {
                if(curChannel >= Sound::Sample::CHANNELS)
                {
                    if(!render(sampleBuf, BYTES_PER_SAMPLE))
                        return false;
                    curChannel = 0;
                }

                *buffer = conversionScale * sampleBuf[curChannel];
                curChannel++;
                return true;
            }

        public:
            Stream(XMPFILE file): StreamBase(),
                file(file),
                curChannel(Sound::Sample::CHANNELS),
                anchorModuleIndex(0){
            }

            void flushSampleBuffer()
            {
                curChannel = Sound::Sample::CHANNELS;
            }

            virtual QWORD seek(QWORD pos)
            {
                flushSampleBuffer(); // this will only work if we read one sample at a time
                return StreamBase::seek(pos);
            }

            long getFloats(float *buffer, DWORD count)
            {
                float* p = buffer;
                while(count)
                {
                    if(!getFloat(p))
                        break;
                    p++;
                    count--;
                }
                return p - buffer;
            }
    };



    //
    // CONFIG CLASS
    //

    class Config
    {
        public:
            enum CanLoad {
                CANNOT_LOAD,
                CAN_LOAD,
                CAN_LOAD_WITH_CHECK
            };

            Config(XMPFUNC_REGISTRY* registry) :
                stdList(supportedExtensions.begin(), supportedExtensions.end()),
                reg(registry),
                whiteListText(getString(PARAM_WHITELIST)),
                blackListText(getString(PARAM_BLACKLIST)),
                extraListText(getString(PARAM_EXTRALIST)),
                loadAll(getBool(PARAM_LOADALL)),
                noCheck(getBool(PARAM_NOCHECK)),
                cacheTags(getBool(PARAM_CACHETAGS)),
                maxSize(getInt(
                    PARAM_MAXSIZE,
                    MAX_FILESIZE == NEG_DWORD
                        ? 0
                        : MAX_FILESIZE / MAXSIZE_MULTIPLIER))
            {
                updateWhiteList();
                updateBlackList();
                updateExtraList();
                updateMaxSize();
            }

            const std::string& getWhiteList() const {return whiteListText;}
            const std::string& getBlackList() const {return blackListText;}
            const std::string& getExtraList() const {return extraListText;}
            bool getLoadAll() const {return loadAll;}
            bool getNoCheck() const {return noCheck;}
            bool getCacheTags() const {return cacheTags;}
            std::string getMaxSizeText() const {return std::to_string(maxSize);}

            void setWhiteList(std::string&& val)
            {
                whiteListText = std::move(val);
                setString(PARAM_WHITELIST, whiteListText);
                updateWhiteList();
            }

            void setBlackList(std::string&& val)
            {
                blackListText = std::move(val);
                setString(PARAM_BLACKLIST, blackListText);
                updateBlackList();
            }

            void setExtraList(std::string&& val)
            {
                extraListText = std::move(val);
                setString(PARAM_EXTRALIST, extraListText);
                updateExtraList();
            }

            void setLoadAll(bool val)
            {
                loadAll = val;
                setBool(PARAM_LOADALL, loadAll);
            }

            void setNoCheck(bool val)
            {
                noCheck = val;
                setBool(PARAM_NOCHECK, noCheck);
            }

            void setCacheTags(bool val)
            {
                cacheTags = val;
                setBool(PARAM_CACHETAGS, cacheTags);
            }

            void setMaxSizeText(std::string&& val)
            {
                try{
                    int res = std::stoi(val);
                    if(res < 0)
                        throw;
                    maxSize = res;
                    setInt(PARAM_MAXSIZE, maxSize);
                    updateMaxSize();
                }catch(...){
                }
            }

            CanLoad canLoadExt(const std::string& ext)
            {
                std::string val = boost::algorithm::to_lower_copy(ext);
                if(blackList.find(val) != blackList.end())
                    return CANNOT_LOAD;
                if(extraList.find(val) != extraList.end())
                    return CAN_LOAD;
                if(whiteList.find(val) != whiteList.end())
                    return CAN_LOAD;
                if(stdList.find(val) != stdList.end())
                    return whiteList.empty() ? CAN_LOAD : CANNOT_LOAD;
                if(loadAll)
                    return noCheck ? CAN_LOAD : CAN_LOAD_WITH_CHECK;
                else
                    return CANNOT_LOAD;
            }

        protected:
            std::string getString(const char* key, const char* def = "")
            {
                DWORD nBytes = reg->GetString(SECTION, key, nullptr, 0);
                if(!nBytes)
                    return def;
                // GetString returns the size without a null terminator
                // but requires the input size to include a terminator.
                // So the return value of GetString needs to be incremented
                // to get a valid buffer size and the number of bytes read.
                nBytes++;
                char buf[nBytes];
                DWORD nGot = reg->GetString(SECTION, key, buf, nBytes) + 1;
                if(nGot != nBytes)
                    return def;
                buf[nBytes-1] = '\0'; // just in case
                return buf;
            }

            int getInt(const char* key, int def = 0)
            {
                int val;
                if(!reg->GetInt(SECTION, key, &val))
                    return def;
                return val;
            }

            bool getBool(const char* key, int def = false)
            {
                return getInt(key, def ? 1 : 0) != 0;
            }

            void setString(const char* key, const std::string& val)
            {
                reg->SetString(SECTION, key, val.data());
            }

            void setInt(const char* key, int val)
            {
                reg->SetInt(SECTION, key, &val);
            }

            void setBool(const char* key, bool val)
            {
                setInt(key, val ? 1 : 0);
            }

            void updateWhiteList(){whiteList = populateExtList(whiteListText);}
            void updateBlackList(){blackList = populateExtList(blackListText);}
            void updateExtraList(){extraList = populateExtList(extraListText);}
            void updateMaxSize(){MAX_FILESIZE = maxSize ? (maxSize * MAXSIZE_MULTIPLIER) : NEG_DWORD;}

            std::unordered_set<std::string> populateExtList(const std::string& text)
            {
                // split into array
                static auto sep = boost::is_any_of(",; \r\n");
                std::string buf = text;
                boost::trim_if(buf, sep);
                if(buf.empty())
                    return std::unordered_set<std::string>();
                std::vector<std::string> list;
                boost::split(list, buf, sep, boost::token_compress_on);

                // normalize extensions
                for(std::string& ext : list)
                {
                    if(ext.compare(0, 1, ".") == 0)
                        ext.erase(0, 1);
                    else if(ext.compare(0, 2, "*.") == 0)
                        ext.erase(0, 2);
                    ext = boost::algorithm::to_lower_copy(ext);
                }

                // remove duplicates
                std::sort(list.begin(), list.end());
                list.erase(std::unique(list.begin(), list.end()), list.end());

                return std::unordered_set<std::string>(list.begin(), list.end());
            }

            std::unordered_set<std::string> stdList;
            XMPFUNC_REGISTRY* reg;
            std::string whiteListText;
            std::string blackListText;
            std::string extraListText;
            bool loadAll;
            bool noCheck;
            bool cacheTags;
            int maxSize;

            std::unordered_set<std::string> whiteList;
            std::unordered_set<std::string> blackList;
            std::unordered_set<std::string> extraList;

            static constexpr const char* SECTION = "XMPZXTUNE";
            static constexpr const char* PARAM_WHITELIST = "whitelist";
            static constexpr const char* PARAM_BLACKLIST = "blacklist";
            static constexpr const char* PARAM_EXTRALIST = "extralist";
            static constexpr const char* PARAM_LOADALL = "loadall";
            static constexpr const char* PARAM_NOCHECK = "nocheck";
            static constexpr const char* PARAM_MAXSIZE = "maxsize";
            static constexpr const char* PARAM_CACHETAGS = "cachetags";
            static constexpr const int MAXSIZE_MULTIPLIER = 1024 * 1024;
    };



    //
    // TAGS CACHE CLASSES
    //

    struct TagsItem {
        std::vector<float> durations;
        std::string tags;

        TagsItem(const Stream& stream)
        {
            const Modules& modules = stream.getModules();
            size_t nModules = modules.size();

            durations.reserve(nModules);
            for(const ModuleBase* module : modules)
                durations.emplace_back(module->getDuration());

            for(const ModuleBase* module : modules)
            {
                tags.append(module->getTags());
                tags.push_back('\0');
            }
        }
    };
    typedef std::unique_ptr<TagsItem> TagsItemPtr;

    class TagsCache
    {
        public:
            TagsItemPtr take(XMPFILE file)
            {
                auto i = map.find(getKey(file));
                if(i == map.end())
                    return nullptr;

                TagsItemPtr item = std::move(i->second);
                map.erase(i);
                return item;
            }

            void put(XMPFILE file, const Stream& stream)
            {
                map.emplace(getKey(file), new TagsItem(stream));
            }

        protected:
            std::string getKey(XMPFILE file)
            {
                return std::string(xmpffile->GetFilename(file))
                        +'\0'
                        +std::to_string(xmpffile->GetSize(file));
            }

            std::map<std::string, TagsItemPtr> map;
    };



    //
    // MISC
    //

    class MutexLocker
    {
        public:
            explicit MutexLocker() {mutex.lock();}
            ~MutexLocker() {mutex.unlock();}

        private:
            static std::mutex mutex;
    };
    std::mutex MutexLocker::mutex;



    //
    // INTERFACE FUNCTIONS
    //

    static Stream* stream = nullptr;
    static HINSTANCE libInstance;
    static std::unique_ptr<Config> cfg;
    static bool loadAllWarningWasShown = false;
    static bool noCheckWarningWasShown = false;
    static bool cacheTagsWarningWasShown = false;
    static TagsCache tagsCache;

    static void WINAPI AboutWnd(HWND win)
    {
        MessageBoxA(
            win,
            "XMPZXTUNE v" AS_STR(VER_MAJ) "." AS_STR(VER_MIN) "." AS_STR(VER_PAT) "\n"
            "Build date: " __DATE__ "\n"
            "Homepage: https://sourceforge.net/projects/xmpzxtune/\n"
            "Based on ZXTune: https://zxtune.bitbucket.io/\n",
            "XMPZXTUNE input plugin",
            MB_OK | MB_ICONINFORMATION
        );
    }

    std::string getConfigDlgItemText(HWND hwndDlg, int nIDDlgItem)
    {
        HWND w = GetDlgItem(hwndDlg, nIDDlgItem);
        if(!w)
            return "";
        int n = GetWindowTextLengthA(w);
        if(!n)
            return "";
        char buf[n+1];
        if(!GetDlgItemTextA(hwndDlg, nIDDlgItem, buf, n+1))
            return "";
        return buf;
    }

    BOOL CALLBACK ConfigWndProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam)
    {
        (void)lParam;

        switch (message)
        {
            case WM_INITDIALOG:
                SetDlgItemTextA(hwndDlg, IDC_EDIT_WHITELIST, cfg->getWhiteList().data());
                SetDlgItemTextA(hwndDlg, IDC_EDIT_BLACKLIST, cfg->getBlackList().data());
                SetDlgItemTextA(hwndDlg, IDC_EDIT_EXTRALIST, cfg->getExtraList().data());
                CheckDlgButton(hwndDlg, IDC_CHECK_LOADALL, cfg->getLoadAll() ? BST_CHECKED : BST_UNCHECKED);
                CheckDlgButton(hwndDlg, IDC_CHECK_NOCHECK, cfg->getNoCheck() ? BST_CHECKED : BST_UNCHECKED);
                CheckDlgButton(hwndDlg, IDC_CHECK_CACHETAGS, cfg->getCacheTags() ? BST_CHECKED : BST_UNCHECKED);
                EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_NOCHECK), cfg->getLoadAll());
                EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_CACHETAGS), cfg->getLoadAll());
                SetDlgItemTextA(hwndDlg, IDC_EDIT_MAXSIZE, cfg->getMaxSizeText().data());
                return TRUE;

            case WM_COMMAND:
                switch(LOWORD(wParam))
                {
                    case IDC_CHECK_LOADALL:
                        if(HIWORD(wParam) == BN_CLICKED)
                        {
                            bool isChecked = IsDlgButtonChecked(hwndDlg, IDC_CHECK_LOADALL) == BST_CHECKED;
                            if(isChecked && !loadAllWarningWasShown)
                            {
                                MessageBoxA(
                                    hwndDlg,
                                    "This option may noticeably slowdown the file list loading.\n"
                                    "Enable the option below this one to eliminate the slowdown.",
                                    "Try to load any non-built-in extension if it's not blacklisted",
                                    MB_OK | MB_ICONWARNING
                                );
                                loadAllWarningWasShown = true;
                            }
                            EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_NOCHECK), isChecked);
                            EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_CACHETAGS), isChecked);
                            return TRUE;
                        }
                        return FALSE;

                    case IDC_CHECK_NOCHECK:
                        if(HIWORD(wParam) == BN_CLICKED)
                        {
                            bool isChecked = IsDlgButtonChecked(hwndDlg, IDC_CHECK_NOCHECK) == BST_CHECKED;
                            if(isChecked)
                                CheckDlgButton(hwndDlg, IDC_CHECK_CACHETAGS, BST_UNCHECKED);
                            if(isChecked && !noCheckWarningWasShown)
                            {
                                MessageBoxA(
                                    hwndDlg,
                                    "This option will eliminate the slowdown\n"
                                    "caused by the option above.\n"
                                    "However, this option will force all selected files\n"
                                    "to be added to the playlist\n"
                                    "even if they can't actually be played.\n"
                                    "Use this option if you don't care about\n"
                                    "possible bogus files in the playlist.",
                                    "...and don't check if the file can actually be opened",
                                    MB_OK | MB_ICONWARNING
                                );
                                noCheckWarningWasShown = true;
                            }
                            return TRUE;
                        }
                        return FALSE;

                    case IDC_CHECK_CACHETAGS:
                        if(HIWORD(wParam) == BN_CLICKED)
                        {
                            bool isChecked = IsDlgButtonChecked(hwndDlg, IDC_CHECK_CACHETAGS) == BST_CHECKED;
                            if(isChecked)
                                CheckDlgButton(hwndDlg, IDC_CHECK_NOCHECK, BST_UNCHECKED);
                            if(isChecked && !cacheTagsWarningWasShown)
                            {
                                MessageBoxA(
                                    hwndDlg,
                                    "This option will speed up the tags retreival,\n"
                                    "but it may increase memory consumption.",
                                    "...and cache tags information",
                                    MB_OK | MB_ICONWARNING
                                );
                                cacheTagsWarningWasShown = true;
                            }
                            return TRUE;
                        }
                        return FALSE;

                    case IDOK:
                        cfg->setWhiteList(getConfigDlgItemText(hwndDlg, IDC_EDIT_WHITELIST));
                        cfg->setBlackList(getConfigDlgItemText(hwndDlg, IDC_EDIT_BLACKLIST));
                        cfg->setExtraList(getConfigDlgItemText(hwndDlg, IDC_EDIT_EXTRALIST));
                        cfg->setLoadAll(IsDlgButtonChecked(hwndDlg, IDC_CHECK_LOADALL) == BST_CHECKED);
                        cfg->setNoCheck(IsDlgButtonChecked(hwndDlg, IDC_CHECK_NOCHECK) == BST_CHECKED);
                        cfg->setCacheTags(IsDlgButtonChecked(hwndDlg, IDC_CHECK_CACHETAGS) == BST_CHECKED);
                        cfg->setMaxSizeText(getConfigDlgItemText(hwndDlg, IDC_EDIT_MAXSIZE));
                        EndDialog(hwndDlg, wParam);
                        return TRUE;

                    case IDCANCEL:
                        EndDialog(hwndDlg, wParam);
                        return TRUE;
                }
        }
        return FALSE;
    }

    static void WINAPI ConfigWnd(HWND win)
    {
        DialogBoxA(libInstance, MAKEINTRESOURCE(IDD_DIALOG_CONFIG), win, &ConfigWndProc);
    }

    // check if a file is playable by this plugin
    // more thorough checks can be saved for the GetFileInfo and Open functions
    static BOOL WINAPI CheckFile(const char *filename, XMPFILE file)
    {
        MutexLocker lock;

        std::string fname(filename);
        std::string ext;
        size_t pos = fname.find_last_of('.');
        if(pos != std::string::npos)
            ext = fname.substr(pos+1);

        switch(cfg->canLoadExt(ext))
        {
            case Config::CAN_LOAD:
                return true;

            case Config::CAN_LOAD_WITH_CHECK:
            {
                Stream stream(file);
                bool ok = stream.init();
                if(!ok)
                    return false;
                if(cfg->getCacheTags())
                    tagsCache.put(file, stream);
                return ok;
            }

            default:
                return false;
        }
    }

    static char *GetModuleTags(const Module* module)
    {
        std::string s = module->getTags();
        int n = s.size() + 1;
        char *tags = (char*)xmpfmisc->Alloc(n);
        return (char*)memcpy(tags, s.c_str(), n);
    }

    // get file info
    // return: the number of subsongs
    static DWORD WINAPI GetFileInfo(const char *filename, XMPFILE file, float **length, char **tags)
    {
        MutexLocker lock;

        (void)filename;

        TagsItemPtr tagsItem;
        if(cfg->getCacheTags())
            tagsItem = tagsCache.take(file);
        if(!tagsItem)
        {
            Stream stream(file);
            if(!stream.init())
                return 0;
            tagsItem.reset(new TagsItem(stream));
        }

        DWORD nModules = tagsItem->durations.size();

        if(length)
        {
            float *p = (float*)xmpfmisc->Alloc(nModules * sizeof(float));
            *length = p;
            for(float duration : tagsItem->durations)
            {
                *p = duration;
                p++;
            }
        }

        if(tags)
        {
            int tagsLength = tagsItem->tags.size();
            *tags = (char*)xmpfmisc->Alloc(tagsLength);
            memcpy(*tags, tagsItem->tags.data(), tagsLength);
        }

        return nModules;
    }

    // open a file for playback
    // return:  0=failed, 1=success, 2=success and XMPlay can close the file
    static DWORD WINAPI Open(const char *filename, XMPFILE file)
    {
        MutexLocker lock;

        (void)filename;

        stream = new Stream(file);
        if(!stream->init())
        {
            delete stream;
            return 0;
        }

        if(cfg->getCacheTags())
            tagsCache.take(file);

        stream->setSingleModuleMode(true);
        xmpfin->SetLength(stream->getDuration(), TRUE);
        return 2;
    }

    // close the file
    static void WINAPI Close()
    {
        delete stream;
        stream = nullptr;
    }

    // set the sample format (in=user chosen format, out=file format if different)
    static void WINAPI SetFormat(XMPFORMAT *form)
    {
        form->res = BYTES_PER_SAMPLE;
        form->chan = Sound::Sample::CHANNELS;
        form->rate = stream->getFrequency();
    }

    // get the tags
    // return NULL to delay the title update when there are no tags (use UpdateTitle to request update when ready)
    static char *WINAPI GetTags()
    {
        return GetModuleTags((Module*)(stream->getCurModule()));
    }

    // get the main panel info text
    static void WINAPI GetInfoText(char *format, char *length)
    {
        const ModuleInfo& info = stream->getInfo();

        if(format)
        {
            format += sprintf(format, "%s", info.type.c_str());
            if(info.nChannels)
                format += sprintf(format, ", %d Channels", info.nChannels);
        }

        if(length)
        {
            length = strrchr(length, 0);
            if(info.nFrames)
                length += sprintf(length, ", %d Frames", info.nFrames);
            if(info.nPositions)
                length += sprintf(length, ", %d Positions", info.nPositions);
        }
    }

    static void AddInfo(char* &buf, const char* heading, int val, const char* units = nullptr)
    {
        if(!val)
            return;
        if(units)
            buf += sprintf(buf, "%s\t%d %s\r", heading, val, units);
        else
            buf += sprintf(buf, "%s\t%d\r", heading, val);
    }

    static void AddInfo(char* &buf, const char* heading, const std::string& val)
    {
        if(val.empty())
            return;
        buf += sprintf(buf, "%s\t%s\r", heading, val.c_str());
    }

    // get text for "General" info window
    // separate headings and values with a tab (\t), end each line with a carriage-return (\r)
    static void WINAPI GetGeneralInfo(char *buf)
    {
        const ModuleInfo& info = stream->getInfo();

        AddInfo(buf, "Type", info.type);
        AddInfo(buf, "Title", info.title);
        AddInfo(buf, "Author", info.author);
        AddInfo(buf, "Program", info.program);
        AddInfo(buf, "Computer", info.computer);
        AddInfo(buf, "Date", info.date);
        AddInfo(buf, "Comment", info.comment);
        AddInfo(buf, "Version", info.version);

        AddInfo(buf, "Channels", info.nChannels);
        AddInfo(buf, "Frames", info.nFrames);
        AddInfo(buf, "Frame Duration", stream->getFrameDuration(), "\xc2\xb5s");
        AddInfo(buf, "Sample Rate", stream->getFrequency(), "Hz");
        AddInfo(buf, "Bytes Per Sample", Sound::Sample::BITS / 8);
        AddInfo(buf, "Initial Tempo", info.initialTempo);
        AddInfo(buf, "Loop Frame", info.loopFrame);
        AddInfo(buf, "Loop Position", info.loopPosition);
        AddInfo(buf, "Positions", info.nPositions);
    }

    // get text for "Message" info window
    // separate tag names and values with a tab (\t), and end each line with a carriage-return (\r)
    static void WINAPI GetMessageText(char *buf)
    {
        const ModuleInfo& info = stream->getInfo();

        AddInfo(buf, "Type", info.type);
        AddInfo(buf, "Title", info.title);
        AddInfo(buf, "Author", info.author);
        AddInfo(buf, "Program", info.program);
        AddInfo(buf, "Computer", info.computer);
        AddInfo(buf, "Date", info.date);
        AddInfo(buf, "Comment", info.comment);
        AddInfo(buf, "Version", info.version);
    }

    // Get the seeking granularity in seconds
    static double WINAPI GetGranularity()
    {
        return 1.f / stream->getFrequency() / BYTES_PER_SAMPLE; // seek in bytes
    }

    // Seek to a position (in granularity units)
    // return the new position in seconds (-1 = failed)
    static double WINAPI SetPosition(DWORD pos)
    {
        switch(pos)
        {
            case (DWORD)XMPIN_POS_LOOP:
            case (DWORD)XMPIN_POS_AUTOLOOP:
            case (DWORD)XMPIN_POS_TAIL:
                return -1;
        }

        if(pos & XMPIN_POS_SUBSONG)
        {
            stream->jumpToModule(LOWORD(pos));
            xmpfin->SetLength(stream->getDuration(), TRUE);
            xmpfin->UpdateTitle(NULL);
        }
        else
        {
            stream->seek(pos);
        }

        return GetGranularity() * stream->getPosition();
    }

    // get some sample data, always floating-point
    // count=number of floats to write (not bytes or samples)
    // return number of floats written. if it's less than requested, playback is ended...
    // so wait for more if there is more to come (use CheckCancel function to check if user wants to cancel)
    static DWORD WINAPI Process(float *buffer, DWORD count)
    {
        return stream->getFloats(buffer, count);
    }

    // get number (and total length) of sub-songs
    static DWORD WINAPI GetSubSongs(float *length)
    {
        stream->setSingleModuleMode(false);
        *length = stream->getDuration();
        stream->setSingleModuleMode(true);
        return stream->getModules().size();
    }

    static const char* getPluginExts()
    {
        static const char* result = nullptr;
        if(result)
            return result;

        assert(supportedExtensions.size());

        static std::string line = "ZXTune-supported format";
        line.push_back('\0');
        for(const auto& ext : supportedExtensions)
        {
            line.append(ext);
            line.push_back('/');
        }
        line.pop_back();

        result = line.data();
        return result;
    }

    // plugin interface
    static XMPIN xmpin={
        0, // cannot stream from net, only use XMPFILE
        "XMPZXTUNE (" AS_STR(VER_MAJ) "." AS_STR(VER_MIN) "." AS_STR(VER_PAT) ")", // plugin name
        getPluginExts(), // supported file extensions
        AboutWnd,
        ConfigWnd,
        CheckFile,
        GetFileInfo,
        Open,
        Close,
        nullptr, // reserved1
        SetFormat,
        GetTags,
        GetInfoText,
        GetGeneralInfo,
        GetMessageText,
        SetPosition,
        GetGranularity,
        nullptr, // GetBuffering
        Process,
        nullptr, // WriteFile
        nullptr, // don't have any "Samples"
        GetSubSongs,
        nullptr, // reserved3
        nullptr, // GetDownloaded
        nullptr, // visname
        nullptr, // VisOpen
        nullptr, // VisClose
        nullptr, // VisSize
        nullptr, // VisRender
        nullptr, // VisRenderDC
        nullptr, // VisButton
        nullptr, // reserved2
        nullptr, // GetConfig
        nullptr // SetConfig
    };

    XMPIN* init(DWORD face, InterfaceProc faceproc)
    {
        static int shownerror=0;
        if(face != XMPIN_FACE)
        {
            // unsupported version
            if(!shownerror)
            {
                MessageBoxA(0, "The XMP-ZXTUNE plugin does not support this version of XMPlay", 0, MB_ICONEXCLAMATION);
                shownerror = 1;
            }
            return nullptr;
        }

        xmpfin = (XMPFUNC_IN*)faceproc(XMPFUNC_IN_FACE);
        xmpfmisc = (XMPFUNC_MISC*)faceproc(XMPFUNC_MISC_FACE);
        xmpffile = (XMPFUNC_FILE*)faceproc(XMPFUNC_FILE_FACE);
        xmpftext = (XMPFUNC_TEXT*)faceproc(XMPFUNC_TEXT_FACE);
        xmpfregistry = (XMPFUNC_REGISTRY*)faceproc(XMPFUNC_REGISTRY_FACE);
        xmpver = xmpfmisc->GetVersion();

        cfg.reset(new Config(xmpfregistry));

        return &xmpin;
    }
}}



//
// LIBRARY INTERFACE
//

extern "C"
__declspec (dllexport)
XMPIN*
WINAPI
XMPIN_GetInterface(DWORD face, InterfaceProc faceproc)
{
    return App::Plugin::init(face, faceproc);
}

__declspec (dllexport) BOOL WINAPI DllMain(HANDLE hDLL, DWORD reason, LPVOID reserved)
{
    (void)reserved;

    switch(reason)
    {
        case DLL_PROCESS_ATTACH:
            App::Plugin::libInstance = (HINSTANCE)hDLL;
            DisableThreadLibraryCalls((HMODULE)hDLL);
            break;
    }

    return TRUE;
}
