/*************************************************************************}
{ libtest.cpp - Test for linked BASSZXTUNE library                        }
{                                                                         }
{ (c) Alexey Parfenov, 2018                                               }
{                                                                         }
{ e-mail: zxed@alkatrazstudio.net                                         }
{                                                                         }
{ This program is free software; you can redistribute it and/or           }
{ modify it under the terms of the GNU General Public License             }
{ as published by the Free Software Foundation; either version 3 of       }
{ the License, or (at your option) any later version.                     }
{                                                                         }
{ This program is distributed in the hope that it will be useful,         }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of          }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        }
{ General Public License for more details.                                }
{                                                                         }
{ You may read GNU General Public License at:                             }
{   http://www.gnu.org/copyleft/gpl.html                                  }
{*************************************************************************/

#include "basszxtune.h"
#include <iostream>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cstring>

static const char* filename = "./chiptunes/AY-3-8910/ym/Kurztech.ym";

int err(const char *msg)
{
    std::cerr << msg << std::endl;
    return 1;
}

template <typename T>
int err(const char *msg, T arg)
{
    std::cerr << msg << ": " << arg << std::endl;
    return 1;
}

int bassErr(const char* funcName)
{
    int code = BASS_ErrorGetCode();
    std::cerr << funcName << " failed: " << code << std::endl;
    return 1;
}

void CALLBACK fileCloseProc(void *user)
{
    fclose(static_cast<FILE*>(user)); // close the file
}

QWORD CALLBACK fileLenProc(void *user)
{
    struct stat s;
    fstat(fileno(static_cast<FILE*>(user)), &s);
    return static_cast<QWORD>(s.st_size); // return the file length
}

DWORD CALLBACK fileReadProc(void *buffer, DWORD length, void *user)
{
    return static_cast<DWORD>(fread(buffer, 1, length, static_cast<FILE*>(user))); // read from file
}

BOOL CALLBACK fileSeekProc(QWORD offset, void *user)
{
    return !fseek(static_cast<FILE*>(user), static_cast<long>(offset), SEEK_SET); // seek to offset
}

int testStream(HSTREAM hStream)
{
    const size_t BUF_SIZE = 1024 * 1024;
    char buf[BUF_SIZE];
    memset(buf, 0, BUF_SIZE);

    DWORD nRead = BASS_ChannelGetData(hStream, buf, BUF_SIZE);
    if(nRead == static_cast<DWORD>(-1))
        return bassErr("BASS_ChannelGetData");
    if(nRead != BUF_SIZE)
        return err("BASS_ChannelGetData returned too few data", nRead);

    bool emptyData = true;
    for(QWORD a=0; a<BUF_SIZE; a++)
    {
        if(buf[a])
        {
            emptyData = false;
            break;
        }
    }
    if(emptyData)
        return err("BASS_ChannelGetData returned empty data");

    if(!BASS_StreamFree(hStream))
        return bassErr("BASS_StreamFree");

    return 0;
}

std::string getFileFolder(const char* filename)
{
    std::string sFilename(filename);
    size_t sepPos = sFilename.find_last_of("/\\");
    return sFilename.substr(0, sepPos);
}

int main(int argc, char* argv[])
{
    (void)argc;

    if(!BASS_Init(0, 44100, 0, 0, nullptr))
        return bassErr("BASS_Init");

    std::string appFolder = getFileFolder(argv[0]);
    std::string samplesFolder = appFolder + "/../../../samples";
    if(chdir(samplesFolder.data()) != 0)
        return err("Can't change dir to 'samples'.");

    std::cout << "Test: BASS_ZXTUNE_StreamCreateFile" << std::endl;

    HSTREAM hStream = BASS_ZXTUNE_StreamCreateFile(false, filename, 0, 0, BASS_STREAM_DECODE);
    if(!hStream)
        return bassErr("BASS_ZXTUNE_StreamCreateFile");

    if(testStream(hStream) != 0)
        return 1;

    std::cout << "Test: BASS_ZXTUNE_StreamCreateFileUser" << std::endl;

    FILE* file = fopen(filename, "rb");
    if(!file)
        return err("Cannot fopen, errno: ", errno);

    BASS_FILEPROCS procs = {
        &fileCloseProc,
        &fileLenProc,
        &fileReadProc,
        &fileSeekProc
    };

    hStream = BASS_ZXTUNE_StreamCreateFileUser(STREAMFILE_NOBUFFER, BASS_STREAM_DECODE, &procs, file);
    if(!hStream)
        return bassErr("BASS_ZXTUNE_StreamCreateFileUser");

    if(testStream(hStream) != 0)
        return 1;

    if(!BASS_Free())
        return bassErr("BASS_Free");

    std::cout << "Done!" << std::endl;

    return 0;
}
