ZXTune
=========

ZXTune is open-source crossplatform chiptunes player.

Official page is https://zxtune.bitbucket.io

This particular fork contains plug-in interfaces to ZXTune core 
(currently BASS add-on and XMPlay plugin).

Generic instructions on how to build a plug-in can be found at:
https://bitbucket.org/z80maniac/zxtune/src/master/apps/plugins/BUILD.TXT
