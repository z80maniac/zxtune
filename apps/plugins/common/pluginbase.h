/*************************************************************************}
{ pluginbase.h - common functionality for all plugins                     }
{                                                                         }
{ (c) Alexey Parfenov, 2015                                               }
{                                                                         }
{ e-mail: zxed@alkatrazstudio.net                                         }
{                                                                         }
{ This program is free software; you can redistribute it and/or           }
{ modify it under the terms of the GNU General Public License             }
{ as published by the Free Software Foundation; either version 3 of       }
{ the License, or (at your option) any later version.                     }
{                                                                         }
{ This program is distributed in the hope that it will be useful,         }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of          }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        }
{ General Public License for more details.                                }
{                                                                         }
{ You may read GNU General Public License at:                             }
{   http://www.gnu.org/copyleft/gpl.html                                  }
{*************************************************************************/

#pragma once

#include <string>
#include <array>
#include <sstream>
#include <tuple>
#include <cstring>

#include <iostream>

#include <cycle_buffer.h>

#include <binary/container.h>
#include <module/holder.h>
#include <core/core_parameters.h>
#include <parameters/container.h>
#include <sound/sound_parameters.h>

#ifdef _WIN32
    #include <wtypes.h>
    typedef unsigned __int64 QWORD;
#else
    typedef uint32_t DWORD;
    typedef uint64_t QWORD;
#endif

namespace App {
namespace Plugin {

    static const std::vector<const char*> supportedExtensions = {
        // AY-3-8910
        "as0",
        "asc",
        "ay",
        "ayc",
        "ftc",
        "gtr",
        "psc",
        "psg",
        "psm",
        "pt1",
        "pt2",
        "pt3",
        "sqt",
        "st1",
        "s",
        "st3",
        "stc",
        "stp",
        "ts",
        // "txt", - supported, but not listed
        "vtx",
        "ym",

        // CO12294
        "rmt",
        "sap",

        // DAC/Amiga
        "ahx",
        "hvl",
        "mod",

        // DAC/Sega
        "dsf",

        // DAC/ZX
        "chi",
        "dmm",
        "dst",
        "d",
        "m",
        "sqd",
        "str",

        // HuC6270
        "hes",

        // LR35902
        "gbs",

        // MOS6581
        "sid",

        // Multi
        "2sf",
        "gsf",
        "gym",
        "kss",
        "mtc",
        "psf",
        "psf2",
        "ssf",
        "usf",
        "vgm",

        // RP2A0X
        "nsf",
        "nsfe",

        // SAA1099
        "cop",

        // SPC700
        "spc",

        // YM2203
        "tf0",
        "tfc",
        "tfd",
        "tfe",

        // archived
        "hrp",
        //"rar", - supported, but not listed
        "rsn",
        "scl",
        "szx",
        "trd",
        "vgz",
        //"zip", - supported, but not listed

        // packed
        "cc3",
        "dsq",
        "esv",
        "fdi",
        "gam",
        "gamplus",
        "$b",
        "$m",
        "hrm",
        "bin",
        "p",
        "lzs",
        "msp",
        "pcd",
        "td0",
        "tlz",
        "tlzp",
        "trs"
    };

    extern DWORD MAX_FILESIZE;

    static const QWORD NEG_QWORD = static_cast<QWORD>(-1);
    static const DWORD NEG_DWORD = static_cast<DWORD>(-1);

    static const int BYTES_PER_SAMPLE = sizeof(Sound::Sample);
    static const int FRAMERATE_MULTIPLIER = 1000 * 1000;

    struct ModuleInfo
    {
        int nPositions = 0;
        int loopPosition = 0;
        int nFrames = 0;
        int loopFrame = 0;
        int nChannels = 0;
        int initialTempo = 0;

        std::string type;
        std::string title;
        std::string author;
        std::string program;
        std::string computer;
        std::string date;
        std::string comment;
        std::string version;
        int64_t versionInt = 0;
        std::string strings;
    };

    class Renderer : public Sound::Receiver
    {
        protected:
            CycleBuffer<Sound::Sample> buffer;
            std::size_t doneSamples;

        public:
            typedef std::shared_ptr<Renderer> Ptr;

            Renderer();
            virtual void ApplyData(Sound::Chunk data);
            virtual void Flush(){}
            std::size_t getCurrentSample() const {return doneSamples;}
            std::size_t getSamples(std::size_t count, Sound::Sample* target);
            std::size_t dropSamples(std::size_t count);
            void reset();
    };

    class Player
    {
        public:
            typedef std::shared_ptr<Player> Ptr;

        protected:
            const Parameters::Container::Ptr params;
            const Renderer::Ptr buffer;
            const Module::Renderer::Ptr renderer;

        public:
            Player(const Module::Holder::Ptr moduleHandle);
            std::size_t render(Sound::Sample* target, std::size_t samples);
            std::size_t seek(std::size_t samples);
            void reset();
            void setParam(const Parameters::NameType& paramName, int paramValue);
            static bool findDefaultValue(const Parameters::NameType& name, Parameters::IntType& value);
            bool getParam(const Parameters::NameType& paramName, int* paramValue);
            Parameters::Container::Ptr getParams() const {return params;}
    };

    class ModuleBase
    {
        private:
            Module::Holder::Ptr handle;
            Player::Ptr player;

            bool fillMainInfo();
            bool fillPropsInfo();

        protected:
            QWORD nBytes;
            QWORD nSamples;
            double nSecs;
            int frequency;
            int frameDuration;
            ModuleInfo info;
            bool tagsReady;
            std::string tags;
            const char* tagsPtr;
            QWORD position;
            QWORD samplePosition;

            virtual void prepareTags(){}
            void setMaxQuality();
            void setMinQuality();
        public:
            QWORD startPos;
            QWORD endPos;

            inline int getFrequency() const {return frequency;}
            inline QWORD getLength() const {return nBytes;}
            inline double getDuration() const {return nSecs;}
            inline int getFrameDuration() const {return frameDuration;}
            //inline ZXTuneHandle getHandle() const {return handle;}

            ModuleBase(Module::Holder::Ptr moduleHandle);
            virtual ~ModuleBase();

            bool init(int _frequency = Parameters::ZXTune::Sound::FREQUENCY_DEFAULT);
            void initPlayer();
            inline const ModuleInfo& getInfo() const {return info;}
            //inline const ZXTuneModuleAttrs &getAttrs() const {return attrs;}
            inline const std::string &getTags() const {return tags;}
            inline const char *getTagsPtr() const {return tagsPtr;}
            inline QWORD getPosition() const {return position;}
            void destroyPlayer();
            Player::Ptr createPlayer();
            Player::Ptr createPlayerRaw();
            QWORD renderSamples(Sound::Sample *buffer, QWORD count);
            QWORD render(void *buffer, QWORD length);
            virtual QWORD seek(QWORD pos);
    };

    typedef std::vector<ModuleBase*> Modules;

    class StreamBase
    {
        protected:
            Binary::Container::Ptr data;
            QWORD nBytes;
            double nSecs;
            int frequency;
            Modules modules;
            ModuleBase* oldModule;
            Modules::iterator curModuleIter;
            Modules::iterator lastModuleIter;
            bool singleModuleMode;

            virtual ModuleBase* createModule(Module::Holder::Ptr handle);
            ModuleBase* addModule(Module::Holder::Ptr handle);
            bool switchModule();
            virtual void onTuneChange(){}

            virtual QWORD getFileSize() = 0;
            virtual QWORD fileRead(char* buffer, QWORD length) = 0;

        public:
            inline ModuleBase* getCurModule() const {return static_cast<ModuleBase*>(*curModuleIter);}
            inline long getCurModuleIndex() const {return curModuleIter - modules.begin();}
            inline const std::string& getTags() const {return getCurModule()->getTags();}
            inline const char* getTagsPtr() const {return getCurModule()->getTagsPtr();}
            inline const ModuleInfo& getInfo() const {return getCurModule()->getInfo();}
            inline int getFrameDuration() const {return getCurModule()->getFrameDuration();}
            inline const Modules& getModules() const {return modules;}
            inline int getFrequency() const {return frequency;}

            QWORD getLength() const;
            double getDuration() const;
            QWORD getPosition() const;

            inline void setSingleModuleMode(bool on) {singleModuleMode = on;}
            inline bool isSingleModuleMode() const {return singleModuleMode;}

            StreamBase();
            virtual ~StreamBase();

            virtual bool init();
            virtual QWORD seek(QWORD pos);
            bool jumpToModule(size_t index);
            virtual QWORD render(void *buffer, QWORD length, bool& atEnd);
            QWORD render(void *buffer, QWORD length);
            virtual QWORD renderSamples(Sound::Sample *buffer, QWORD nSamples, bool &atEnd);
            QWORD renderSamples(Sound::Sample *buffer, QWORD nSamples);

        private:
            std::vector<Module::Holder::Ptr> detectModules(const Binary::Container::Ptr data);
    };

}}
