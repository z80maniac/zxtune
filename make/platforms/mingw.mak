makebin_name = $(1).exe
makelib_name = lib$(1).a
makedyn_name = $(1).dll
makeobj_name = $(1).o
makeres_cmd = $(mingw.execprefix)windres -O coff --input $(1) --output $(2) $(addprefix -D,$(DEFINES))

host ?= windows
compiler=gcc

ifdef release
$(platform).ld.flags += -Wl,-subsystem,$(if $(have_gui),windows,console)
else
$(platform).ld.flags += -Wl,-subsystem,console
endif

$(platform).ld.flags += -Wl,--add-stdcall-alias -Wl,--enable-stdcall-fixup
