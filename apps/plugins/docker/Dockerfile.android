FROM debian:buster-slim

RUN apt update -yq && \
    apt upgrade -yq && \
    apt install -yq wget unzip bzip2 make && \
    rm -rf /var/lib/apt/lists/*

ARG ndk=20
ARG toolchain=/toolchain
RUN mkdir ndk && \
    cd ndk && \
        wget -q https://dl.google.com/android/repository/android-ndk-r${ndk}-linux-x86_64.zip -O ndk.zip && \
        unzip -q ndk.zip && \
        mv android-ndk-r${ndk}/toolchains/llvm/prebuilt/linux-x86_64 ${toolchain} && \
    cd .. && \
    rm -rf ndk

ARG boost=1.70.0
RUN mkdir boost && \
    cd boost && \
        bash -c "wget -q https://dl.bintray.com/boostorg/release/${boost}/source/boost_\${boost//./_}.tar.bz2 -O - | tar -xj" && \
        mv boost_*/boost ${toolchain}/sysroot/usr/include/ && \
    cd .. && \
    rm -rf boost

ARG bass=2.4
RUN mkdir bass && \
    cd bass && \
        bash -c "wget -q http://www.un4seen.com/stuff/bass\${bass//./}-android.zip -O bass.zip" && \
        unzip -q bass.zip && \
        mv libs/armeabi-v7a/libbass.so ${toolchain}/sysroot/usr/lib/arm-linux-androideabi/ && \
        mv libs/arm64-v8a/libbass.so ${toolchain}/sysroot/usr/lib/aarch64-linux-android/ && \
        mv libs/x86/libbass.so ${toolchain}/sysroot/usr/lib/i686-linux-android/ && \
        mv libs/x86_64/libbass.so ${toolchain}/sysroot/usr/lib/x86_64-linux-android/ && \
    cd .. && \
    rm -rf bass

ENV PATH="/toolchain/bin:${PATH}"
