libraries.common = analysis \
                   binary binary_compression binary_format \
                   core core_plugins_archives core_plugins_players \
                   devices_aym devices_beeper devices_dac devices_fm devices_saa devices_z80 \
                   formats_archived formats_archived_multitrack formats_chiptune formats_packed formats_multitrack \
                   l10n_stub \
                   module module_players \
                   parameters \
                   sound strings \
                   tools

libraries.3rdparty = asap gme he ht hvl lazyusf2 lhasa lzma mgba sidplayfp snesspc unrar vio2sf xmp z80ex zlib

libraries.windows := oldnames

ifndef VER_MAJ
$(error VER_MAJ is not defined)
endif
ifndef VER_MIN
$(error VER_MIN is not defined)
endif
ifndef VER_PAT
$(error VER_PAT is not defined)
endif

defines += VER_MAJ=$(VER_MAJ) VER_MIN=$(VER_MIN) VER_PAT=$(VER_PAT)

depends := apps/plugins/common
libraries := app_plugins_common
