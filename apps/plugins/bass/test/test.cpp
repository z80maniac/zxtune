/*************************************************************************}
{ test.cpp - Tests for BASS plugin                                        }
{                                                                         }
{ (c) Alexey Parfenov, 2016                                               }
{                                                                         }
{ e-mail: zxed@alkatrazstudio.net                                         }
{                                                                         }
{ This program is free software; you can redistribute it and/or           }
{ modify it under the terms of the GNU General Public License             }
{ as published by the Free Software Foundation; either version 3 of       }
{ the License, or (at your option) any later version.                     }
{                                                                         }
{ This program is distributed in the hope that it will be useful,         }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of          }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        }
{ General Public License for more details.                                }
{                                                                         }
{ You may read GNU General Public License at:                             }
{   http://www.gnu.org/copyleft/gpl.html                                  }
{*************************************************************************/

#include <iostream>

#include <filesystem>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

#include "../bass.h"
#include "../consts.h"

#ifdef __linux__
    static const char *pluginFilename = "./libbasszxtune.so";
#endif
#ifdef _WIN32
    static const char *pluginFilename = "./basszxtune.dll";
#endif
#ifdef __APPLE__
    static const char *pluginFilename = "./libbasszxtune.dylib";
#endif

using namespace std::filesystem;

int err(const char *msg)
{
    std::cerr << msg << std::endl;
    return 1;
}

template <typename T>
int err(const char *msg, T arg)
{
    std::cerr << msg << ": " << arg << std::endl;
    return 1;
}

int bassErr(const char* funcName)
{
    int code = BASS_ErrorGetCode();
    std::cerr << funcName << " failed: " << code << std::endl;
    return 1;
}

// http://stackoverflow.com/a/19453043
struct recursive_directory_range
{
    typedef recursive_directory_iterator iterator;
    recursive_directory_range(path p) : p_(p) {}

    iterator begin() { return recursive_directory_iterator(p_); }
    iterator end() { return recursive_directory_iterator(); }

    path p_;
};

void stringToUpper(std::string& s)
{
    for(auto &c: s)
        c = static_cast<char>(::toupper(c));
}

void CALLBACK onSyncChange(HSYNC handle, DWORD channel, DWORD data, void *user)
{
    (void)handle;
    (void)channel;
    (void)data;

    (*(static_cast<QWORD*>(user)))++;
}

void CALLBACK onSyncEnd(HSYNC handle, DWORD channel, DWORD data, void *user)
{
    (void)handle;
    (void)channel;
    (void)data;

    *((static_cast<bool*>(user))) = true;
}

struct FileEntry
{
    std::string filename;
    std::string ext;

    FileEntry(std::string&& filename, std::string&& ext) :
        filename(std::move(filename)),
        ext(std::move(ext))
    {
    }

    FileEntry(FileEntry&& other) :
        filename(std::move(other.filename)),
        ext(std::move(other.ext))
    {
    }
};

int main(int argc, char* argv[])
{
    (void)argc;

    path path(argv[0]);
    current_path(canonical(path.parent_path()));

    if(!BASS_Init(0, 44100, 0, nullptr, nullptr))
        return bassErr("BASS_Init");

    HPLUGIN hPlug = BASS_PluginLoad(pluginFilename, 0);
    if(!hPlug)
        return bassErr("BASS_PluginLoad");

    current_path(current_path().string()+"/../../../samples");
    std::vector<FileEntry> entries;
    for(auto it : recursive_directory_range("."))
    {
        std::string filename = it.path().string();
        std::string ext = it.path().extension().string();
        if(!ext.empty())
        {
            stringToUpper(ext);
            entries.emplace_back(FileEntry(std::move(filename), std::move(ext)));
        }
    }

    const BASS_PLUGININFO *info = BASS_PluginGetInfo(hPlug);
    if(!info)
        return bassErr("BASS_PluginGetInfo");

    std::vector<std::string> extensions;
    const std::string extPrefix("*.");
    for(DWORD a=0; a<info->formatc; a++)
    {
        std::string extStr(info->formats[a].exts);
        std::vector<std::string> exts;
        boost::split(exts, extStr, boost::is_any_of(";"), boost::token_compress_on);
        for(const std::string& ext : exts)
        {
            int res = ext.compare(0, extPrefix.size(), extPrefix);
            if(res)
                return err("Invalid extension", ext.c_str());
            std::string dotExt = ext.substr(extPrefix.size()-1);
            stringToUpper(dotExt);
            extensions.emplace_back(std::move(dotExt));
        }
    }

    if(extensions.empty())
        return err("No extensions specified");

    std::vector<FileEntry> musicEntries;
    for(std::string& ext : extensions)
    {
        bool found = false;
        for(FileEntry& entry : entries)
        {
            if(entry.ext.compare(ext) == 0)
            {
                musicEntries.emplace_back(std::move(entry));
                found = true;
            }
        }
        if(!found)
            return err("No samples found for extension", ext.c_str());
    }

    const QWORD BUF_SIZE = 1024*1024; // 1 MiB
    char buf[BUF_SIZE];

    const char* firstFilename = musicEntries[0].filename.c_str();

    DWORD maxFileSize = BASS_GetConfig(BASS_CONFIG_ZXTUNE_MAXFILESIZE);
    if(!maxFileSize)
        return bassErr("BASS_GetConfig");

    if(!BASS_SetConfig(BASS_CONFIG_ZXTUNE_MAXFILESIZE, 1024))
        return bassErr("BASS_SetConfig");

    HSTREAM hStream = BASS_StreamCreateFile(false, firstFilename, 0, 0, BASS_STREAM_DECODE);
    if(hStream)
        return err("BASS_CONFIG_ZXTUNE_MAXFILESIZE had no effect");

    if(!BASS_SetConfig(BASS_CONFIG_ZXTUNE_MAXFILESIZE, maxFileSize))
        return bassErr("BASS_SetConfig");

    hStream = BASS_StreamCreateFile(false, firstFilename, 0, 0, BASS_STREAM_DECODE);
    if(!hStream)
        return bassErr("BASS_StreamCreateFile (first)");

    QWORD nBytes = BASS_ChannelGetLength(hStream, BASS_POS_BYTE);
    if(!nBytes)
        return bassErr("BASS_ChannelGetLength");

    double nSecs = BASS_ChannelBytes2Seconds(hStream, nBytes);
    if(nSecs <= 0)
        return bassErr("BASS_ChannelBytes2Seconds");

    if(!BASS_StreamFree(hStream))
        return bassErr("BASS_StreamFree");

    hStream = BASS_StreamCreateFile(false, firstFilename, 0, 0, BASS_STREAM_DECODE | BASS_SAMPLE_FLOAT);
    if(!hStream)
        return bassErr("BASS_StreamCreateFile (float)");

    nBytes = BASS_ChannelGetLength(hStream, BASS_POS_BYTE);
    if(!nBytes)
        return bassErr("BASS_ChannelGetLength (float)");

    double nSecs2 = BASS_ChannelBytes2Seconds(hStream, nBytes);
    if(nSecs2 <= 0)
        return bassErr("BASS_ChannelBytes2Seconds (float)");

    if(std::abs(nSecs - nSecs2) > 0.1)
        return err("Float channel length differs from 16-bit channel length");

    double targetSecs = nSecs2 - 1;
    QWORD nSeekBytes = BASS_ChannelSeconds2Bytes(hStream, targetSecs);
    if(!nSeekBytes)
        return bassErr("BASS_ChannelSeconds2Bytes (float)");

    if(!BASS_ChannelSetPosition(hStream, nSeekBytes, BASS_POS_BYTE))
        return bassErr("BASS_ChannelSetPosition (float)");

    QWORD nRealSeekBytes = BASS_ChannelGetPosition(hStream, BASS_POS_BYTE);
    if(nRealSeekBytes == static_cast<QWORD>(-1))
        return bassErr("BASS_ChannelGetPosition (float)");

    if(nSeekBytes != nRealSeekBytes)
        return err("Seeked to a wrong byte position (float), diff: ", nRealSeekBytes - nSeekBytes);

    double realSecs = BASS_ChannelBytes2Seconds(hStream, nRealSeekBytes);
    if(std::abs(realSecs - targetSecs) > 0.1)
        return err("Seeked to a wrong time position, diff: ", realSecs - targetSecs);

    if(!BASS_StreamFree(hStream))
        return bassErr("BASS_StreamFree");

    size_t filesCount = musicEntries.size();
    for(size_t fileIndex = 0; fileIndex < filesCount; fileIndex++)
    {
        const FileEntry& entry = musicEntries[fileIndex];
        const char* fname = entry.filename.c_str();
        std::cout << "File [" << (fileIndex+1) << "/" << filesCount << "]: " << fname << std::endl;

#ifdef __APPLE__
        if(entry.ext == ".SSF") // known bug
        {
            std::cout << "Skipping SSF on OSX" << std::endl;
            continue;
        }
#endif

        HSTREAM hStream = BASS_StreamCreateFile(false, fname, 0, 0, BASS_STREAM_DECODE);
        if(!hStream)
            return bassErr("BASS_StreamCreateFile");

        QWORD nBytes = BASS_ChannelGetLength(hStream, BASS_POS_BYTE);
        if(!nBytes)
            return bassErr("BASS_ChannelGetLength");

        double nSecs = BASS_ChannelBytes2Seconds(hStream, nBytes);
        if(nSecs <= 0)
            return bassErr("BASS_ChannelBytes2Seconds");

        QWORD nTunes = BASS_ChannelGetLength(hStream, BASS_POS_ZXTUNE_SUB_COUNT);
        if(!nTunes)
            return bassErr("BASS_ChannelGetLength (BASS_POS_ZXTUNE_SUB_COUNT)");

        QWORD nTunes2 = BASS_ChannelGetLength(hStream, BASS_POS_OGG);
        if(!nTunes2)
            return bassErr("BASS_ChannelGetLength (BASS_POS_OGG)");

        if(nTunes != nTunes2)
            return err("BASS_POS_ZXTUNE_SUB_COUNT does not return the same value as BASS_POS_OGG");

        if(!BASS_ChannelGetTags(hStream, BASS_TAG_OGG))
            return bassErr("BASS_ChannelGetTags");

        QWORD nBytesSum = 0;
        for(QWORD a=0; a<nTunes; a++)
        {
            QWORD nTuneBytes = BASS_ChannelGetLength(hStream, BASS_POS_ZXTUNE_SUB_LENGTH + static_cast<DWORD>(a));
            if(!nTuneBytes)
                return err("Cannot get subtune length", a);
            nBytesSum += nTuneBytes;
            if(!BASS_ChannelGetTags(hStream, BASS_TAG_ZXTUNE_SUB_OGG + static_cast<DWORD>(a)))
                return err("Cannot get subtune tags", a);
        }
        if(nBytesSum != nBytes)
            return err("Sum of subtune lengths does not equal the total length");

        double targetSecs = nSecs - 1;
        QWORD nSeekBytes = BASS_ChannelSeconds2Bytes(hStream, targetSecs);
        if(!nSeekBytes)
            return bassErr("BASS_ChannelSeconds2Bytes");

        if(!BASS_ChannelSetPosition(hStream, nSeekBytes, BASS_POS_BYTE))
            return bassErr("BASS_ChannelSetPosition");

        QWORD nRealSeekBytes = BASS_ChannelGetPosition(hStream, BASS_POS_BYTE);
        if(nRealSeekBytes == static_cast<QWORD>(-1))
            return bassErr("BASS_ChannelGetPosition");

        if(nSeekBytes != nRealSeekBytes)
            return err("Seeked to a wrong byte position, diff: ", nRealSeekBytes - nSeekBytes);

        double realSecs = BASS_ChannelBytes2Seconds(hStream, nRealSeekBytes);
        if(std::abs(realSecs - targetSecs) > 0.1)
            return err("Seeked to a wrong time position, diff: ", realSecs - targetSecs);

        if(!BASS_ChannelSetPosition(hStream, 0, BASS_POS_BYTE))
            return bassErr("BASS_ChannelSetPosition (start)");

        QWORD nChange = 0;
        HSYNC hSyncChange = BASS_ChannelSetSync(hStream, BASS_SYNC_OGG_CHANGE, 0, &onSyncChange, &nChange);
        if(!hSyncChange)
            return bassErr("BASS_ChannelSetSync (BASS_SYNC_OGG_CHANGE)");

        bool endTriggered = false;
        HSYNC hSyncEnd = BASS_ChannelSetSync(hStream, BASS_SYNC_END, 0, &onSyncEnd, &endTriggered);
        if(!hSyncEnd)
            return bassErr("BASS_ChannelSetSync (BASS_SYNC_OGG_CHANGE)");

        DWORD nRead = BASS_ChannelGetData(hStream, buf, BUF_SIZE);
        if(nRead == static_cast<DWORD>(-1))
            return bassErr("BASS_ChannelGetData (start)");
        if(nRead != BUF_SIZE)
            return err("BASS_ChannelGetData returned too few data", nRead);

        bool emptyData = true;
        for(QWORD a=0; a<BUF_SIZE; a++)
        {
            if(buf[a])
            {
                emptyData = false;
                break;
            }
        }
        if(emptyData)
            return err("BASS_ChannelGetData returned empty data");

        QWORD nBytesLeft = nBytes - nRead;
        while(true)
        {
            DWORD nRead = BASS_ChannelGetData(hStream, buf, BUF_SIZE);
            if(nRead == static_cast<DWORD>(-1))
            {
                int errCode = BASS_ErrorGetCode();
                if(errCode == BASS_ERROR_ENDED)
                    nRead = 0;
                else
                    return bassErr("BASS_ChannelGetData");
            }
            if(!nRead)
            {
                if(!nBytesLeft)
                    break;
                else
                {
                    int exitCode = err("BASS_ChannelGetData has stopped before all data returned", nBytesLeft);
                    if(entry.ext == ".MTC" && nBytesLeft == 3528)
                        break; // known bug
                    else
                        return exitCode;
                    break;
                }
            }
            else
            {
                if(nRead > nBytesLeft)
                    return err("BASS_ChannelGetData returned too much data", nRead - nBytesLeft);
                nBytesLeft = nBytesLeft - nRead;
            }
        }

        if(!endTriggered)
            return err("BASS_SYNC_END has not been triggered");
        if(nChange != (nTunes-1))
            return err("BASS_SYNC_OGG_CHANGE has been called less/more times than the number of tune changes", nChange);
        if(!BASS_ChannelRemoveSync(hStream, hSyncChange))
            return bassErr("BASS_ChannelRemoveSync (BASS_SYNC_OGG_CHANGE)");
        if(!BASS_ChannelRemoveSync(hStream, hSyncEnd))
            return bassErr("BASS_ChannelRemoveSync (BASS_SYNC_END)");

        if(!BASS_StreamFree(hStream))
            return bassErr("BASS_StreamFree");
    }

    if(!BASS_PluginFree(hPlug))
        return bassErr("BASS_PluginFree");

    if(!BASS_Free())
        return bassErr("BASS_Free");

    std::cout << "Done!" << std::endl;

    return 0;
}
