#!/bin/bash
set -e
cd "${0%`basename $0`}"

function err()
{
    echo "$1" 1>&2
    exit 1
}

function checkdir()
{
    if [[ ! -d "$1" ]]
    then
        err "Not found: $1"
    fi
}

[[ -z $1 ]] && err "Usage: [VER=x.y.z] [PLATFORM=<platform>] [ARCH=<arch>] [...] ./build.sh <plugin subfolder>"

MAKECOMMAND=make
MAKEPARAMS=()
INCLUDE_DIRS=()
LIB_DIRS=()

cd ../..
MAKEDIR="apps/plugins/$1"
checkdir "$MAKEDIR"

if [[ -z $VER ]]
then
    if [[ -f $MAKEDIR/VERSION ]]
    then
        VER="$(< "$MAKEDIR/VERSION")"
    fi
fi

if [[ -n $VER ]]
then
    IFS="." read -ra VERS <<< "$VER"
    VER_MAJ="${VERS[0]}"
    VER_MIN="${VERS[1]}"
    VER_PAT="${VERS[2]}"
    [[ -z $VER_MAJ || -z $VER_MIN || -z $VER_PAT ]] && err "VER needs to be in format: x.y.z"
    MAKEPARAMS+=("VER_MAJ='$VER_MAJ'" "VER_MIN='$VER_MIN'" "VER_PAT='$VER_PAT'")
fi

if [[ -z $PLATFORM ]]
then
    UNAME="$(uname -s)"
    case "$UNAME" in
        Linux*)
            PLATFORM=linux
            ;;

        Darwin*)
            PLATFORM=darwin
            ;;

        MINGW*)
            PLATFORM=mingw
            ;;

        *)
            err "Unknown platform: $UNAME"
            ;;
    esac
fi

case "$PLATFORM" in
    linux)
        NPROC=$(nproc)
        TOOLCHAINS_ROOT="$(dirname "$(which gcc)")"
        THEPLATFORM="$PLATFORM"

        if [[ -z $ARCH ]]
        then
            if [[ $(uname -m) == "x86_64" ]]
            then
                ARCH=x86_64
            else
                ARCH=i686
            fi
        else
            case "$ARCH" in
                i686)
                    ;;

                x86_64)
                    ;;

                *)
                    err "Unsupported: ARCH=$ARCH"
                    ;;
            esac
        fi

        THEARCH="$ARCH"
        MAKEPARAMS+=("$THEPLATFORM.$THEARCH.boost.libs.model=")
        ;;

    android)
        NPROC=$(nproc)
        THEPLATFORM="$PLATFORM"
        [[ -z $ARCH ]] && err "Required: ARCH"
        [[ -z $NDK_TOOLCHAIN ]] && err "Required: NDK_TOOLCHAIN"

        case "$ARCH" in
            arm_v7a)
                THEARCH=armeabi-v7a
                PREFIX=arm-linux-androideabi
                COMPILERPREFIX=armv7a-linux-androideabi16
                ;;

            arm64_v8a)
                THEARCH=arm64-v8a
                PREFIX=aarch64-linux-android
                COMPILERPREFIX=aarch64-linux-android21
                ;;

            i686)
                THEARCH=x86
                PREFIX=i686-linux-android
                COMPILERPREFIX=i686-linux-android16
                ;;

            x86_64)
                THEARCH=x86_64
                PREFIX=x86_64-linux-android
                COMPILERPREFIX=x86_64-linux-android21
                ;;

            *)
                err "Unsupported: ARCH=$ARCH"
                ;;
        esac

        if [[ $(uname -m) == x86_64 ]]
        then
            PREBUILT=linux-x86_64
        else
            PREBUILT=linux-x86
        fi

        TOOLCHAINS_ROOT="$NDK_TOOLCHAIN/bin"

        MAKEPARAMS+=(
            "android.$THEARCH.execprefix=${PREFIX}-"
            "android.$THEARCH.execprefix.compiler=${COMPILERPREFIX}-"
        )
        ;;

    darwin)
        NPROC=$(sysctl -n hw.ncpu)
        THEARCH=x86_64
        THEPLATFORM="$PLATFORM"
        TOOLCHAINS_BASE="$(xcode-select --print-path)"
        TOOLCHAINS_ROOT="$TOOLCHAINS_BASE/Toolchains/XcodeDefault.xctoolchain/usr/bin"
        INCLUDE_DIRS+=("/usr/local/include")
        _CXX_FLAGS="-isysroot $TOOLCHAINS_BASE/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk"
        ;;

    mingw)
        NPROC=$(nproc)
        if [[ $MSYSTEM == "MINGW64" ]]
        then
            THEARCH=x86_64
        else
            THEARCH=i686
        fi

        THEPLATFORM="$PLATFORM"
        TOOLCHAINS_ROOT="$(dirname $(which gcc))"
        MAKEPARAMS+=("host=linux" "mingw.execprefix=")
        INCLUDE_DIRS+=("/usr/include" "/${MSYSTEM,,}/include")
        LIB_DIRS+=("$TOOLCHAINS_ROOT")
        ;;

    *)
        err "Unknown: PLATFORM=$PLATFORM"
        ;;
esac

checkdir "$TOOLCHAINS_ROOT"
PATH="$TOOLCHAINS_ROOT:$PATH"

if [[ -z $MODE ]]
then
    MODE=release
fi

if [[ $2 == "show" ]]
then
    PREPEND=echo
    ARG=""
else
    PREPEND=""
    ARG="$2"
    set -x
fi

$PREPEND $MAKECOMMAND \
    -j$NPROC \
    "$MODE"="$MODE" \
    platform="$THEPLATFORM" \
    arch="$THEARCH" \
    toolchains.root="$TOOLCHAINS_ROOT" \
    $THEPLATFORM.$THEARCH.toolchain="$TOOLCHAINS_ROOT" \
    prebuilt.dir=/ \
    cxx_flags="$_CXX_FLAGS $CXX_FLAGS -Wno-deprecated-declarations -DNO_DEBUG_LOGS" \
    ld_flags="$LD_FLAGS" \
    ar_flags="$AR_FLAGS" \
    "${MAKEPARAMS[@]}" \
    libraries.dirs.${THEPLATFORM}="${LIB_DIRS[*]}" \
    includes.dirs.${THEPLATFORM}="${INCLUDE_DIRS[*]}" \
    -C "$MAKEDIR" $ARG "${@:3}"

if [[ $2 != "show" ]]
then
    set +x
    echo "Done!"
fi
